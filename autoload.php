<?php

use Kaemmelot\Tools\Dumper\CurrentDumper;
use Kaemmelot\Tools\Dumper\StaticResourceProvider;
use Kaemmelot\StackTrace\StackTrace;

if (!defined("ENT_HTML5"))
    define("ENT_HTML5", 0); // PHP 5.3 compatibility

CurrentDumper::init();
StaticResourceProvider::startup();

/**
 * Print a value as dump.
 * @param mixed $value          The value to dump.
 * @param bool  $die            True to stop after printing.
 * @param bool  $showStackTrace True to show where this function was called.
 * @return mixed The value (if die == false).
 */
function kdump($value, $die = false, $showStackTrace = true)
{
    CurrentDumper::dump($value, $showStackTrace);
    if ($die)
        die;

    return $value;
}

/**
 * Print a value once as dump.
 * @param mixed        $value          The value to dump.
 * @param bool|integer $conditionOrId  True/False or a specific id.
 * @param bool         $showStackTrace True to show where this function was called.
 * @return mixed The value.
 */
function kdumpOnce($value, $conditionOrId = true, $showStackTrace = true)
{
    if ($conditionOrId !== false)
        CurrentDumper::dumpOnce($value, (($conditionOrId === true) ? null : $conditionOrId), $showStackTrace);
    return $value;
}

/**
 * Print a value as dump if condition is true.
 * @param mixed $value          The value to dump.
 * @param bool  $condition      True/False.
 * @param bool  $die            True to stop after printing.
 * @param bool  $showStackTrace True to show where this function was called.
 * @return mixed The value (if die == false).
 */
function kdumpIf($value, $condition, $die = false, $showStackTrace = true)
{
    if ($condition)
        kdump($value, $die, $showStackTrace);
    return $value;
}

/**
 * Print the current stacktrace.
 * @param bool $die True to stop after printing.
 */
function ktrace($die = false)
{
    CurrentDumper::dump(CurrentDumper::getStackTraceFilterInstance()->filter(StackTrace::getCurrent(true)), false);
    if ($die)
        die;
}

/**
 * Print the current stacktrace only once.
 * @param bool|integer $conditionOrId True/False or a specific id.
 */
function ktraceOnce($conditionOrId = true)
{
    if ($conditionOrId !== false)
        CurrentDumper::dumpOnce(CurrentDumper::getStackTraceFilterInstance()->filter(StackTrace::getCurrent(true)),
            (($conditionOrId === true) ? null : $conditionOrId), false);
}

/**
 * Print the current stacktrace if condition is true.
 * @param bool $condition True/False.
 * @param bool $die       True to stop after printing.
 */
function ktraceIf($condition, $die = false)
{
    if ($condition)
        ktrace($die);
}
