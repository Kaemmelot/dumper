# Dumper for PHP

This is my playground where I develop an alternative to var_dump(), print_r() or echoing things in php to debug values within websites.  
Just use `kdump($value)` to generate a javascript and css overlay with information about your $value.

## Why this tool?
Don't you like how [Symfonys var-dumper](https://symfony.com/doc/current/components/var_dumper.html "symfony/var-dumper documentation") breaks the website template when dumping? Don't you like the unusual and sometimes confusing look?  
Does [Krumo](http://krumo.sourceforge.net/ "Krumo on SourceForge") look too simplistic and too old for you? Do you miss protected and private instance variables when using it?  
Do you hate long `var_dumps($value)` that are hardly readable and where [xdebug](https://xdebug.org/docs/all_functions#xdebug_var_dump "Xdebug documentation") stops with `...` when it get's to the important part?  
Do you need more functionality and especially more than `echo $value`?

Than compare these things to this dumper [here](http://dumper.kaemmelot.de/demos) and see why you should give it a try.

## How to install
### Composer
Just use
```
composer require --dev kaemmelot/dumper
```
in your project root and be sure that
```
require __DIR__ . '/vendor/autoload.php';
```
is done before any output is send. Otherwise this dumper could break.  
Now you're ready to go.

### Old-fashioned way
As all composer libraries you can use this dumper without composer by downloading and extracting it in your project root (don't forget the dependencies) and by creating an autoloader or by requiring all .php files.

Need a ready-to-use solution for an autoloader where you can insert all PSR-0 and PSR-4 `composer.json` entries? Have a look [here](https://bitbucket.org/snippets/Kaemmelot/ao847 "simpleStaticAutoloader.php").

## How to use
That's easy. There are two functions you may use: `kdump($value)` and `ktrace()`.  
The first is used to have a look into values and the second helps finding out how the code got here (i.e. print a [stacktrace](http://php.net/manual/en/function.debug-print-backtrace.php "Backtrace in PHP")). Both of them have a conditional and a single-execution version.  
Have a look at the [rough documentation](http://dumper.kaemmelot.de/docs "Documentation") to see how this works.

## Possible questions
### Why is there no finished documentation or unit tests?
This is still under construction and because I made this all for myself in the first place documentation and unit tests will be added later when I got time for it and when my most important TODOs are implemented.

But `kdump($value)` and `ktrace()` are documented of course. Have a look into [`autoload.php`](autoload.php "autoload.php") or at the [documentation page](http://dumper.kaemmelot.de/docs "Documentation") if you need to know more.

### Will there be more features?
I got a big list of ideas what I could do next. My goal is to implement most of them and make this a really useful tool without making it too complicated to use. I'm also thinking of other tools I could provide in additional packages. But I cannot guarantee that all this will happen.

### Why is this tool so slow?
I have to admit, I may have messed up a few things and I have to improve that, especially some front-end performance aspects. Nevertheless this tool should be fast enough for development purposes.
