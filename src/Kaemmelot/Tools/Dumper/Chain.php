<?php

namespace Kaemmelot\Tools\Dumper;

interface Chain
{
    function getNext($element);

    function getFirst();
}
