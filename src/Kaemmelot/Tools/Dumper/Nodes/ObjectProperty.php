<?php

namespace Kaemmelot\Tools\Dumper\Nodes;

use Kaemmelot\Tools\Dumper\Chain;
use Kaemmelot\Tools\Dumper\PlainObjectConverters\PlainObjectConvertable;
use Kaemmelot\Tools\Dumper\PlainObjectConverters\PlainObjectConverter;

class ObjectProperty implements PlainObjectConvertable
{
    /**
     * @var string
     */
    private $name;

    /**
     * Can be "private", "public" or "protected".
     * @var string
     */
    private $visibility;

    /**
     * @var bool
     */
    private $isStatic;

    /**
     *
     * @var string
     */
    private $declaringClass;

    /**
     *
     * @var Node
     */
    private $value;

    public function __construct($name, $visibility, $isStatic, $declaringClass, Node $value)
    {
        $this->name = $name;
        $this->visibility = $visibility;
        $this->isStatic = $isStatic;
        $this->declaringClass = $declaringClass;
        $this->value = $value;
    }

    /**
     * @return boolean
     */
    public function isComplex()
    {
        return true;
    }

    /**
     * @param Chain $converterChain
     * @return object
     */
    public function convertToPlainObject(Chain $converterChain)
    {
        /* @var $converter PlainObjectConverter */
        $converter = $converterChain->getFirst();

        $value = $converter->convertToPlainObject($this->value, $converterChain);

        return (object) array(
            "name"           => $this->name,
            "visibility"     => $this->visibility,
            "isStatic"       => $this->isStatic,
            "declaringClass" => $this->declaringClass,
            "value"          => $value);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getVisibility()
    {
        return $this->visibility;
    }

    public function getIsStatic()
    {
        return $this->isStatic;
    }

    public function getDeclaringClassName()
    {
        return $this->declaringClassName;
    }

    public function getValue()
    {
        return $this->value;
    }
}
