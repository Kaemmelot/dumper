<?php

namespace Kaemmelot\Tools\Dumper\Nodes;

use Kaemmelot\Tools\Dumper\Chain;
use Kaemmelot\Tools\Dumper\PlainObjectConverters\PlainObjectConvertable;

class UnknownNode extends Node implements PlainObjectConvertable
{
    /**
     * @return boolean
     */
    public function isComplex()
    {
        return false;
    }

    /**
     * @param Chain $converterChain
     * @return object
     */
    public function convertToPlainObject(Chain $converterChain)
    {
        return (object) array("@type" => "unknown", "id" => $this->getId(), "type" => $this->getType());
    }
}
