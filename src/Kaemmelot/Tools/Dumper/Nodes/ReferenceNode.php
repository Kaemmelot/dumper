<?php

namespace Kaemmelot\Tools\Dumper\Nodes;

use Exception;
use Kaemmelot\Tools\Dumper\Chain;
use Kaemmelot\Tools\Dumper\PlainObjectConverters\PlainObjectConvertable;

class ReferenceNode extends Node implements PlainObjectConvertable
{
    /**
     * @var int[]
     */
    private static $referenceIds = array();

    /**
     * @var Node
     */
    private $referencedNode;

    public function __construct(Node $node = null)
    {
        parent::__construct("null");

        $this->referencedNode = $node;
    }

    public function createId()
    {
        if (!isset(self::$referenceIds[$this->referencedNode->getId()]))
            self::$referenceIds[$this->referencedNode->getId()] = 0;

        $this->setIdOnce($this->referencedNode->getId() . "~" .
                         ++self::$referenceIds[$this->referencedNode->getId()]);
    }

    /**
     * @param Node $node
     * @throws Exception if reference was already set
     */
    public function setReferencedNodeOnce(Node $node)
    {
        if ($this->referencedNode !== null)
            throw new Exception("Referenced node was already set");

        $this->referencedNode = $node;
    }

    /**
     * @return Node
     */
    public function getReferencedNode()
    {
        return $this->referencedNode;
    }

    /**
     * @return boolean
     */
    public function isComplex()
    {
        return true;
    }

    /**
     * @param Chain $converterChain
     * @return object
     * @throws Exception If no referencedNode has been set yet or it has no id.
     */
    public function convertToPlainObject(Chain $converterChain)
    {
        if (($this->referencedNode === null) || ($this->referencedNode->getId() ===
                                                 null)
        )
            throw new Exception("Invalid state: " . (($this->referencedNode === null)
                                    ? "Referenced node not set" : "Referenced node has no id"));

        if ($this->getId() === null)
            $this->createId();

        return (object) array("@type"        => "reference", "id" => $this->getId(),
                              "referencedId" => $this->referencedNode->getId());
    }
}
