<?php

namespace Kaemmelot\Tools\Dumper\Nodes;

use Kaemmelot\Tools\Dumper\Chain;
use Kaemmelot\Tools\Dumper\PlainObjectConverters\PlainObjectConvertable;

class ObjectNode extends Node implements PlainObjectConvertable
{
    /**
     *
     * @var string
     */
    private $class;

    /**
     *
     * @var ObjectProperty[]
     */
    private $properties;

    /**
     * @var string|null
     */
    private $outline;

    /**
     * @param string           $class
     * @param ObjectProperty[] $properties
     * @param string|null      $outline
     */
    public function __construct($class, array $properties, $outline = null)
    {
        parent::__construct("object");

        $this->class = $class;
        $this->properties = $properties;
        $this->outline = $outline;
    }

    /**
     * @return boolean
     */
    public function isComplex()
    {
        return true;
    }

    /**
     * @param Chain $converterChain
     * @return object
     */
    public function convertToPlainObject(Chain $converterChain)
    {
        $plainProperties = array();

        foreach ($this->properties as $p)
            $plainProperties[] = $p->convertToPlainObject($converterChain);

        return (object) array("@type"   => "object", "id" => $this->getId(),
                              "type"    => $this->class, "properties" => $plainProperties,
                              "outline" => $this->outline);
    }
}
