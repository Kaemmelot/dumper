<?php

namespace Kaemmelot\Tools\Dumper\Nodes;

abstract class Node
{
    /**
     * @var string
     */
    private $id = null;

    /**
     * @var string
     */
    private $type;

    /**
     * @param string $type
     */
    public function __construct($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @throws Exception if id was already set
     */
    public function setIdOnce($id)
    {
        if ($this->id !== null)
            throw new Exception("Id was already set (was " . $this->id . ")");

        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
