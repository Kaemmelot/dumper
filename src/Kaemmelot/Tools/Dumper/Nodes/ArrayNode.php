<?php

namespace Kaemmelot\Tools\Dumper\Nodes;

use Kaemmelot\Tools\Dumper\Chain;
use Kaemmelot\Tools\Dumper\PlainObjectConverters\PlainObjectConvertable;

class ArrayNode extends Node implements PlainObjectConvertable
{
    /**
     * @var ArrayElement[]
     */
    private $array;

    public function __construct(array $value)
    {
        parent::__construct("array");
        $this->array = $value;
    }

    /**
     * @return boolean
     */
    public function isComplex()
    {
        return true;
    }

    /**
     * @param Chain $converterChain
     * @return object
     */
    public function convertToPlainObject(Chain $converterChain)
    {
        $items = array();

        foreach ($this->array as $element)
            $items[] = $element->convertToPlainObject($converterChain);

        return (object) array("@type" => "array", "id" => $this->getId(), "items" => $items);
    }
}
