<?php

namespace Kaemmelot\Tools\Dumper\Nodes;

use Kaemmelot\Tools\Dumper\Chain;
use Kaemmelot\Tools\Dumper\PlainObjectConverters\PlainObjectConvertable;

class AbortDepthNode extends Node implements PlainObjectConvertable
{
    /**
     * @var int
     */
    private $depth;

    public function __construct($depth)
    {
        parent::__construct("AbortDepth");

        $this->depth = $depth;
    }

    /**
     * @return boolean
     */
    public function isComplex()
    {
        return false;
    }

    /**
     * @param Chain $converterChain
     * @return object
     */
    public function convertToPlainObject(Chain $converterChain)
    {
        return (object) array("@type" => "abortDepth", "id" => $this->getId(), "depth" => $this->depth);
    }
}
