<?php

namespace Kaemmelot\Tools\Dumper\Nodes;

use Kaemmelot\Tools\Dumper\Chain;
use Kaemmelot\Tools\Dumper\PlainObjectConverters\PlainObjectConvertable;

class PrimitiveNode extends Node implements PlainObjectConvertable
{
    /**
     * @var mixed
     */
    private $value;

    public function __construct($value)
    {
        parent::__construct(\gettype($value));

        $this->value = $value;
        if (\is_float($value))
        {
            if (\is_nan($value))
                $this->value = "NaN";
            elseif (\is_infinite($value))
                $this->value = "Infinite";
        }
        elseif ($value === null)
        {
            $this->value = "null";
        }
        elseif (\is_string($value))
        {
            if (\function_exists("mb_detect_encoding"))
            {
                if (\class_exists("\\UConverter"))
                    $this->value =
                        \UConverter::transcode($value, "UTF-8", \mb_detect_encoding($value, \mb_detect_order(), true));
                else if (\function_exists("iconv"))
                    $this->value = \iconv(\mb_detect_encoding($value, \mb_detect_order(), true), "UTF-8", $value);
            }
            // else no converting
        }
    }
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return boolean
     */
    public function isComplex()
    {
        return false;
    }

    /**
     * @param Chain $converterChain
     * @return object
     */
    public function convertToPlainObject(Chain $converterChain)
    {
        return (object) array("@type" => "primitive", "id" => $this->getId(),
                              "value" => $this->value, "type" => $this->getType());
    }
}
