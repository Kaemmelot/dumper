<?php

namespace Kaemmelot\Tools\Dumper\Nodes;

use Kaemmelot\Tools\Dumper\Chain;
use Kaemmelot\Tools\Dumper\PlainObjectConverters\PlainObjectConvertable;
use Kaemmelot\Tools\Dumper\PlainObjectConverters\PlainObjectConverter;

class StackTraceNode extends Node implements PlainObjectConvertable
{
    /**
     * @var CallFrameNode[]
     */
    private $callFrameNodes;

    /**
     * @param CallFrameNode[] $callNodes
     */
    public function __construct(array $callNodes)
    {
        parent::__construct("stackTrace");
        $this->callFrameNodes = $callNodes;
    }

    /**
     * @param Chain $converterChain
     * @return object
     */
    public function convertToPlainObject(Chain $converterChain)
    {
        $callFrames = array();
        /* @var $plainConverter PlainObjectConverter */
        $plainConverter = $converterChain->getFirst();
        foreach ($this->callFrameNodes as $callFrameNode)
            $callFrames[] = $plainConverter->convertToPlainObject($callFrameNode,
                                                                  $converterChain); // IMPORTANT! Nodes always have to be converted by using the chain
        return (object) array("@type"      => $this->getType(),
                              "id"         => $this->getId(),
                              "callFrames" => $callFrames);
    }

    /**
     * @return boolean
     */
    public function isComplex()
    {
        return true;
    }

    /**
     * @return CallFrameNode[]
     */
    public function getCallFrameNodes()
    {
        return $this->callFrameNodes;
    }
}
