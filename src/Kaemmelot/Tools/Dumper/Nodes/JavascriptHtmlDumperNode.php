<?php

namespace Kaemmelot\Tools\Dumper\Nodes;

use Kaemmelot\Tools\Dumper\Chain;
use Kaemmelot\Tools\Dumper\PlainObjectConverters\LinearPlainObjectConverter;
use Kaemmelot\Tools\Dumper\PlainObjectConverters\PlainObjectConvertable;
use Kaemmelot\Tools\Dumper\PlainObjectConverters\PlainObjectConverter;

class JavascriptHtmlDumperNode implements PlainObjectConvertable
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var Node
     */
    private $dumpedNode;

    /**
     * @var LinearPlainObjectConverter
     */
    private $linearPlainObjectConverter;

    /**
     * @var StackTraceNode|null
     */
    private $trace;

    /**
     * @var string|null
     */
    private $callingLine;

    /**
     * @param string                     $id
     * @param Node                       $dumpedNode
     * @param LinearPlainObjectConverter $linearPlainObjectConverter
     * @param StackTraceNode|null        $trace
     * @param string|null                $callingLine
     */
    public function __construct($id, Node $dumpedNode, LinearPlainObjectConverter $linearPlainObjectConverter,
        StackTraceNode $trace = null, $callingLine)
    {
        $this->id = $id;
        $this->dumpedNode = $dumpedNode;
        $this->linearPlainObjectConverter = $linearPlainObjectConverter;
        $this->trace = $trace;
        $this->callingLine = $callingLine;
    }

    /**
     * @return boolean
     */
    function isComplex()
    {
        return true;
    }

    /**
     * @param Chain $converterChain
     * @return object
     */
    function convertToPlainObject(Chain $converterChain)
    {
        /* @var $plainConverter PlainObjectConverter */
        $plainConverter = $converterChain->getFirst();

        return (object) array(
            "@type"       => "javascriptHtmlDumper",
            "id"          => $this->id,
            "root"        => $plainConverter->convertToPlainObject($this->dumpedNode, $converterChain),
            "trace"       => ($this->trace !== null) ?
                $plainConverter->convertToPlainObject($this->trace, $converterChain) : null,
            "callingLine" => $this->callingLine,
            "nodes"       => $this->linearPlainObjectConverter->getElements()
        );
    }
}
