<?php

namespace Kaemmelot\Tools\Dumper;

use Exception;

final class StaticResourceProvider
{
    /**
     * @var StaticResourceProvider
     */
    private static $instance = null;

    /**
     * @var string
     */
    private $resourcePath;

    /**
     * @var string
     */
    private $resourcePackagePath;

    /**
     * @var array<string,StaticResourcePackage>
     */
    private $resourcePackages = array();

    /**
     * @var array<string,boolean>
     */
    private $providedResources = array();

    /**
     * @var array<string,boolean>
     */
    private $providedResourcePackages = array();

    private static function notFound($die = true)
    {
        \header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found", true, 404);
        if ($die)
            die;
    }

    public static function startup()
    {
        if ($path = \filter_input(INPUT_GET, "kaemmelot-dumper-resource", FILTER_SANITIZE_STRING))
        {
            // Path must start with separator
            $basePath = \realpath(__DIR__ . "/../../../../resources");
            $path = \realpath($basePath . $path);

            if (($path === false) && (\strpos($path, $basePath) !== 0) || !\is_readable($path)) // non-existing, forbidden or non-readable file
                self::notFound();

            if (\substr($path, -3) === ".js")
                \header("content-type: text/javascript");
            else if (\substr($path, -4) === ".css")
                \header("content-type: text/css");
            else if (\substr($path, -4) === ".map")
                \header("content-type: text/json");
            else if (\substr($path, -5) === ".html")
                \header("content-type: text/html");
            else if (\substr($path, -4) === ".gif")
                \header("content-type: image/gif");

            \readfile($path);
            exit;
        }
        else if ($packageName = \filter_input(INPUT_GET, "kaemmelot-dumper-resource-package", FILTER_SANITIZE_STRING))
        {
            if (isset(self::getInstance()->resourcePackages[$packageName])) // TODO config because real project and packages are not loaded yet
            {
                \header("content-type: text/html");
                self::getInstance()->provideResources($packageName);
                exit;
            }
            else
                self::notFound();
        }
    }

    /**
     * @return StaticResourceProvider
     */
    public static function getInstance()
    {
        if (self::$instance === null)
            self::$instance = new StaticResourceProvider();

        return self::$instance;
    }

    private function __construct()
    {
        $this->resourcePath = \filter_input(INPUT_SERVER, "SCRIPT_NAME") . "?kaemmelot-dumper-resource=";
        $this->resourcePackagePath = \filter_input(INPUT_SERVER, "SCRIPT_NAME") . "?kaemmelot-dumper-resource-package=";
        $this->getResourcePackage("default")
            ->addJavascript("default",
                            "var kaemmelotDumperBasePath = '" . \htmlspecialchars($this->resourcePath, ENT_COMPAT | ENT_HTML5, "UTF-8") . "';");
    }

    /**
     * @return string
     */
    public function getResourcePath()
    {
        return $this->resourcePath;
    }

    /**
     * @param string $packageName
     * @return StaticResourcePackage
     */
    public function getResourcePackage($packageName)
    {
        if (!isset($this->resourcePackages[$packageName]))
        {
            $this->resourcePackages[$packageName] = new StaticResourcePackage($packageName, $this->resourcePath);
            $this->providedResourcePackages[$packageName] = false;
        }
        return $this->resourcePackages[$packageName];
    }

    private function echoResourcePackage($packageName)
    {
        if (!isset($this->resourcePackages[$packageName]))
            throw new Exception("Package '" . $packageName . "' is undefined");

        if (!$this->providedResourcePackages[$packageName])
        {
            foreach ($this->resourcePackages[$packageName]->getResources() as $resourceName => $content)
            {
                if (!isset($this->providedResources[$resourceName]))
                {
                    echo $content;
                    $this->providedResources[$resourceName] = true;
                }
            }
            $this->providedResourcePackages[$packageName] = true;
        }
    }

    /**
     * @param string $packageName
     */
    public function provideResources($packageName)
    {
        $this->echoResourcePackage("default");
        $this->echoResourcePackage($packageName);
    }

    /**
     * @param string $packageName
     * @return string
     */
    public function getPackageListingPath($packageName)
    {
        return $this->resourcePackagePath . $packageName;
    }
}
