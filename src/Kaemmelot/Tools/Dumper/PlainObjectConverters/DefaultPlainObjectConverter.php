<?php

namespace Kaemmelot\Tools\Dumper\PlainObjectConverters;

use Exception;
use Kaemmelot\Tools\Dumper\Chain;
use Kaemmelot\Tools\Dumper\Nodes\Node;

class DefaultPlainObjectConverter implements PlainObjectConverter
{
    /**
     * @param Node  $node
     * @param Chain $converterChain
     * @return boolean
     */
    public function isComplex(Node $node, Chain $converterChain)
    {
        /* @var $node PlainObjectConvertable */
        if ($node instanceof PlainObjectConvertable)
            return $node->isComplex();
        else
            throw new Exception("Not implemented");
    }

    /**
     * @param Node  $node
     * @param Chain $converterChain
     * @return object
     */
    public function convertToPlainObject(Node $node, Chain $converterChain)
    {
        /* @var $node PlainObjectConvertable */
        if ($node instanceof PlainObjectConvertable)
            return $node->convertToPlainObject($converterChain);
        else
            throw new Exception("Not implemented");
    }
}
