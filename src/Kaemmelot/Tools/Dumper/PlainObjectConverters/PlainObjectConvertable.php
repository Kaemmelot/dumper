<?php

namespace Kaemmelot\Tools\Dumper\PlainObjectConverters;

use Kaemmelot\Tools\Dumper\Chain;

interface PlainObjectConvertable
{
    /**
     * @return boolean
     */
    function isComplex();

    /**
     * @param Chain $converterChain
     * @return object
     */
    function convertToPlainObject(Chain $converterChain);
}
