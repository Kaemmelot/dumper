<?php

namespace Kaemmelot\Tools\Dumper\PlainObjectConverters;

use Kaemmelot\Tools\Dumper\Chain;
use Kaemmelot\Tools\Dumper\Nodes\Node;

interface PlainObjectConverter
{
    /**
     * @param Node  $node
     * @param Chain $converterChain
     * @return boolean
     */
    function isComplex(Node $node, Chain $converterChain);

    /**
     * @param Node  $node
     * @param Chain $converterChain
     * @return object
     */
    function convertToPlainObject(Node $node, Chain $converterChain);
}
