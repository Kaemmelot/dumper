<?php

namespace Kaemmelot\Tools\Dumper\PlainObjectConverters;

use Kaemmelot\Tools\Dumper\Chain;
use Kaemmelot\Tools\Dumper\Nodes\Node;
use Kaemmelot\Tools\Dumper\Nodes\ReferenceNode;

class LinearPlainObjectConverter implements PlainObjectConverter
{
    /**
     * @var Node[]
     */
    private $elements = array();

    /**
     * @param boolean $sort For easier debugging
     * @return Node[]
     */
    public function getElements($sort = false)
    {
        if ($sort)
            \ksort($this->elements);

        return $this->elements;
    }

    /**
     * @param Node  $node
     * @param Chain $converterChain
     * @return boolean
     */
    public function isComplex(Node $node, Chain $converterChain)
    {
        /* @var $next PlainObjectConverter */
        $next = $converterChain->getNext($this);

        return $next->isComplex($node, $converterChain);
    }

    /**
     * @param Node  $node
     * @param Chain $converterChain
     * @return object
     */
    public function convertToPlainObject(Node $node, Chain $converterChain)
    {
        /* @var $first PlainObjectConverter */
        $first = $converterChain->getFirst();
        /* @var $next PlainObjectConverter */
        $next = $converterChain->getNext($this);

        if (!($node instanceof ReferenceNode) && $first->isComplex($node,
                                                                   $converterChain)
        )
        {
            if (!isset($this->elements[$node->getId()]))
            {
                $this->elements[$node->getId()] = true; // Stop recursion
                $this->elements[$node->getId()] = $next->convertToPlainObject($node,
                                                                              $converterChain);
            }

            $ref = new ReferenceNode($node);

            return $ref->convertToPlainObject($converterChain);
        }
        else if (($node instanceof ReferenceNode) && !isset($this->elements[$node->getReferencedNode()->getId()]))
        {
            // Add referenced elements
            $first->convertToPlainObject($node->getReferencedNode(),
                                         $converterChain);

            return $node->convertToPlainObject($converterChain);
        }
        else // Simple or ReferenceNode
            return $next->convertToPlainObject($node, $converterChain);
    }
}
