<?php

namespace Kaemmelot\Tools\Dumper;

use Exception;

final class StaticResourcePackage
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $basePath;

    /**
     * @var array<string,string>
     */
    private $resources = array();

    /**
     * @param string $name
     * @param string $basePath
     */
    public function __construct($name, $basePath)
    {
        $this->name = $name;
        $this->basePath = $basePath . (($basePath[\strlen($basePath) - 1] !== "/") ? "/" : "");
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getBasePath()
    {
        return $this->basePath;
    }

    /**
     * @param array <string,string> $attributes
     * @return string
     */
    private function attributesToString($attributes)
    {
        $result = "";
        foreach ($attributes as $name => $content)
            $result .= \htmlspecialchars($name, ENT_COMPAT | ENT_HTML5, "UTF-8") . '="' .
                       \htmlspecialchars($content, ENT_COMPAT | ENT_HTML5, "UTF-8") . '" ';

        return $result;
    }

    /**
     * @param string                $file
     * @param array <string,string> $attributes
     * @return $this
     * @throws Exception
     */
    public function addJavascriptFile($file, $attributes = array())
    {
        if (isset($this->resources[$file]))
            throw new Exception("Resource $file already exists");

        if (!isset($attributes["type"])) // Default type
            $attributes["type"] = "text/javascript";
        $attributes["src"] = $this->basePath . $file;

        $this->resources[$file] = '<script ' . $this->attributesToString($attributes) . '></script>';

        return $this;
    }

    /**
     * @param  string                $name
     * @param  string                $content
     * @param  array <string,string> $attributes
     * @return $this
     * @throws Exception
     */
    public function addJavascript($name, $content, $attributes = array())
    {
        if (isset($this->resources[$name]))
            throw new Exception("Resource $name already exists");

        if (!isset($attributes["type"])) // Default type
            $attributes["type"] = "text/javascript";

        $this->resources[$name] = '<script ' . $this->attributesToString($attributes) . '>' .
                                  \htmlspecialchars($content, ENT_COMPAT | ENT_HTML5, "UTF-8") . '</script>';

        return $this;
    }

    /**
     * @param  string                $file
     * @param  array <string,string> $attributes
     * @return $this
     * @throws Exception
     */
    public function addStylesheetFile($file, $attributes = array())
    {
        if (isset($this->resources[$file]))
            throw new Exception("Resource $file already exists");

        if (!isset($attributes["type"])) // Default type
            $attributes["type"] = "text/css";
        $attributes["rel"] = "stylesheet";
        $attributes["href"] = $this->basePath . $file;

        $this->resources[$file] = '<link ' . $this->attributesToString($attributes) . '/>';

        return $this;
    }

    /**
     * @param string                $file
     * @param boolean               $hidden
     * @param array <string,string> $attributes
     * @return $this
     * @throws Exception
     */
    public function addImage($file, $hidden = true, $attributes = array())
    {
        if (isset($this->resources[$file]))
            throw new Exception("Resource $file already exists");

        $attributes["src"] = $this->basePath . $file;
        if ($hidden)
        {
            if (isset($attributes["style"]))
                $attributes["style"] .= ";display:none !important;";
            else
                $attributes["style"] = "display:none !important;";
        }

        $this->resources[$file] = '<img ' . $this->attributesToString($attributes) . '/>';

        return $this;
    }

    /**
     * @return array<string, string>
     */
    public function getResources()
    {
        return $this->resources;
    }
}
