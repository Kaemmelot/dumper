<?php

namespace Kaemmelot\Tools\Dumper;

use Exception;
use Kaemmelot\StackTrace\Filter;
use Kaemmelot\StackTrace\FilterCriteria\PathFilterCriterion;
use Kaemmelot\StackTrace\StackTrace;
use Kaemmelot\Tools\Dumper\Dumpers\Dumper;
use Kaemmelot\Tools\Dumper\Dumpers\JavascriptHtmlDumper;
use Kaemmelot\Tools\Dumper\PlainObjectConverters\DefaultPlainObjectConverter;
use Kaemmelot\Tools\Dumper\PlainObjectConverters\PlainObjectConverter;
use Kaemmelot\Tools\Dumper\ValueToNodeConverters\ArrayValueToNodeConverter;
use Kaemmelot\Tools\Dumper\ValueToNodeConverters\BreadthFirstSearchValueToNodeConverter;
use Kaemmelot\Tools\Dumper\ValueToNodeConverters\DepthFirstSearchValueToNodeConverter;
use Kaemmelot\Tools\Dumper\ValueToNodeConverters\ObjectValueToNodeConverter;
use Kaemmelot\Tools\Dumper\ValueToNodeConverters\PrimitiveValueToNodeConverter;
use Kaemmelot\Tools\Dumper\ValueToNodeConverters\TraceValueToNodeConverter;
use Kaemmelot\Tools\Dumper\ValueToNodeConverters\UnknownNodeConverter;
use Kaemmelot\Tools\Dumper\ValueToNodeConverters\ValueToNodeConverter;

class CurrentDumper
{
    /**
     * @var Dumper|null
     */
    private static $instance = null;

    /**
     * @var Filter|null
     */
    private static $stackTraceFilter = null;

    /**
     * @var array<string, bool>
     */
    private static $dumpedIds = array();

    /**
     * @param Dumper $instance
     */
    public static function setInstance(Dumper $instance)
    {
        self::$instance = $instance;
    }

    /**
     *
     * @return Dumper
     * @throws Exception
     */
    public static function getInstance()
    {
        if (self::$instance === null)
            throw new Exception("Missing instance for Dumper");

        return self::$instance;
    }

    /**
     * @param Filter $filter
     */
    public static function setStackTraceFilter(Filter $filter)
    {
        self::$stackTraceFilter = $filter;
    }

    /**
     * @return Filter
     */
    public static function getNewInternalStackTraceFilterInstance()
    {
        $filter = new Filter();
        // Remove internals, but leave entry frame
        $filter->exclude(new PathFilterCriterion(\realpath(__DIR__ . '\..\..\..\..')), Filter::AFTER_FIRST, true);

        return $filter;
    }

    /**
     * @return Filter
     */
    public static function getStackTraceFilterInstance()
    {
        if (self::$stackTraceFilter === null)
            self::$stackTraceFilter = self::getNewInternalStackTraceFilterInstance();

        return self::$stackTraceFilter;
    }

    public static function dump($value, $showStackTrace = true)
    {
        self::getInstance()->dump($value, $showStackTrace);
    }

    public static function dumpOnce($value, $id = null, $showStackTrace = true)
    {
        $id = ($id === null) ?
            "s" . self::getStackTraceFilterInstance()->filter(StackTrace::getCurrent(true))->toIdentifier() :
            "u" . $id;
        if (!isset(self::$dumpedIds[$id]))
            self::dump($value, $showStackTrace);
        self::$dumpedIds[$id] = true;
    }

    /**
     * @param ValueToNodeConverter[] $valueToNodeConverters
     * @param PlainObjectConverter[] $plainObjectConverters
     * @param int                    $maxDepth
     * @param bool                   $ignoreDepthIfLastNode Go on for simple nodes even if $maxDepth is exceeded
     * @param boolean                $useBFS                Use breadth-first search instead of depth-first search
     * @param string                 $cssStyle
     */
    public static function init(array $valueToNodeConverters = array(),
                                array $plainObjectConverters = array(),
                                $maxDepth = 15, $ignoreDepthIfLastNode = true,
                                $useBFS = true, $cssStyle = "default")
    {
        // TODO add configuration (+file)
        if (\count($valueToNodeConverters) === 0)
        {
            if ($useBFS)
                $valueToNodeConverters[] = new BreadthFirstSearchValueToNodeConverter($maxDepth,
                                                                                      $ignoreDepthIfLastNode);
            else
                $valueToNodeConverters[] = new DepthFirstSearchValueToNodeConverter($maxDepth,
                                                                                    $ignoreDepthIfLastNode);
            $valueToNodeConverters[] = new PrimitiveValueToNodeConverter();
            $valueToNodeConverters[] = new ArrayValueToNodeConverter();
            $valueToNodeConverters[] = new TraceValueToNodeConverter();
            $valueToNodeConverters[] = new ObjectValueToNodeConverter();
            $valueToNodeConverters[] = new UnknownNodeConverter();
        }
        $valueToNodeConverterChain = new ArrayChain($valueToNodeConverters);

        if (\count($plainObjectConverters) === 0)
        {
            $plainObjectConverters[] = new DefaultPlainObjectConverter();
        }
        $plainObjectConverterChain = new ArrayChain($plainObjectConverters);
        self::setInstance(new JavascriptHtmlDumper($valueToNodeConverterChain,
                                                   $plainObjectConverterChain,
                                                   $cssStyle));
    }
}
