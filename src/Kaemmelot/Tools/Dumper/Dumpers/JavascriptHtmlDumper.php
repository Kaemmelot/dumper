<?php

namespace Kaemmelot\Tools\Dumper\Dumpers;

use Kaemmelot\StackTrace\Filter;
use Kaemmelot\StackTrace\StackTrace;
use Kaemmelot\Tools\Dumper\ArrayChain;
use Kaemmelot\Tools\Dumper\CurrentDumper;
use Kaemmelot\Tools\Dumper\Nodes\JavascriptHtmlDumperNode;
use Kaemmelot\Tools\Dumper\PlainObjectConverters\LinearPlainObjectConverter;
use Kaemmelot\Tools\Dumper\StaticResourceProvider;
use Kaemmelot\Tools\Dumper\ValueToNodeConverters\ValueToNodeConverter;

class JavascriptHtmlDumper implements Dumper
{
    const USE_MINIFIED_SOURCES = true; // TODO config

    /**
     * @var StaticResourceProvider
     */
    private $resourceProvider;

    /**
     * @var ArrayChain
     */
    private $valueToNodeConverterChain;

    /**
     * @var ArrayChain
     */
    private $plainObjectConverterChain;

    /**
     * @var string
     */
    private $cssStyle;

    /**
     * @var Filter|null
     */
    private $stackTraceFilter;

    /**
     * @var boolean
     */
    private $printCallingLine;

    /**
     * @var array<string, int>
     */
    private $usedIds = array();

    /**
     * @param ArrayChain                  $valueToNodeConverterChain
     * @param ArrayChain                  $plainObjectConverterChain
     * @param string                      $cssStyle
     * @param StaticResourceProvider|null $resourceProvider
     * @param Filter|null                 $stackTraceFilter
     * @param boolean                     $printCallingLine
     */
    public function __construct(ArrayChain $valueToNodeConverterChain,
        ArrayChain $plainObjectConverterChain,
        $cssStyle,
        StaticResourceProvider $resourceProvider = null,
        Filter $stackTraceFilter = null,
        $printCallingLine = true)
    {
        $this->valueToNodeConverterChain = $valueToNodeConverterChain;
        $this->plainObjectConverterChain = $plainObjectConverterChain;
        $this->cssStyle = $cssStyle;
        $this->resourceProvider = ($resourceProvider !== null) ? $resourceProvider
            : StaticResourceProvider::getInstance();
        $this->resourceProvider->getResourcePackage("JavascriptHtmlDumperExternal")
                               //->addJavascriptFile("lib/require.min.js", array( // This causes problems when requirejs is already used
                               //    "data-main" => $this->resourceProvider->getResourcePath() . "/dist/js/external" . (self::USE_MINIFIED_SOURCES ? ".min" : "")))
                               ->addJavascriptFile("lib/require.min.js")
                               ->addJavascript("startup", "window.kaemmelotRequire=window.requirejs.config({baseUrl:kaemmelotDumperBasePath+'/dist/js'" .
                                                          ",context:'javascriptHtmlDumper',paths:{'domReady':'../../lib/domReady.min'," .
                                                          "'jquery':'../../lib/jquery.min'},map:{'*':{'jquery':'JQueryWrapper'},'JQueryWrapper'" .
                                                          ":{'jquery':'jquery'}}});window.kaemmelotDefine=window.define;" .
                                                          "window.define=window.kaemmelotOldDefine;window.require=window.kaemmelotOldRequire;window.requirejs=window.kaemmelotOldRequirejs;" . // Leave no trace
                                                          "window.kaemmelotRequire(['external.min'],function(){});") // TODO only .min works
                               ->addImage("images/wait.gif", true);

        $this->resourceProvider->getResourcePackage("JavascriptHtmlDumperInternal")
                               ->addJavascriptFile("lib/require.min.js")
                               ->addJavascript("startup", "window.kaemmelotRequire=window.requirejs.config({baseUrl:kaemmelotDumperBasePath+'/dist/js'" .
                                                          ",context:'javascriptHtmlDumper',paths:{'domReady':'../../lib/domReady.min','jquery':'../../lib/jquery.min'" .
                                                          ",'ResizeSensor':'../../lib/ResizeSensor.min','velocity':'../../lib/velocity.min'}," .
                                                          "shim:{'velocity':['jquery'],'internal.min':['ResizeSensor']}});window.kaemmelotDefine=window.define;" .
                                                          "window.kaemmelotRequire(['internal.min'],function(){});")
                               ->addStylesheetFile("dist/css/internal." . (self::USE_MINIFIED_SOURCES ? "min." : "") . "css");

        $this->resourceProvider->getResourcePackage("JavascriptHtmlDumperInternalControls")
                               ->addJavascriptFile("lib/require.min.js")
                               ->addJavascript("startup", "window.kaemmelotRequire=window.requirejs.config({baseUrl:kaemmelotDumperBasePath+'/dist/js'" .
                                                          ",context:'javascriptHtmlDumper',paths:{'domReady':'../../lib/domReady.min','jquery':'../../lib/jquery.min'" .
                                                          "}});window.kaemmelotDefine=window.define;window.kaemmelotRequire(['internal-controls.min'],function(){});")
                               ->addStylesheetFile("dist/css/internal-controls." . (self::USE_MINIFIED_SOURCES ? "min." : "") . "css");
        $this->stackTraceFilter = $stackTraceFilter;
        $this->printCallingLine = $printCallingLine;
    }

    public function dump($value, $showCallStack = true)
    {
        if (!\headers_sent())
        {
            \header_remove("Content-Length"); // Some cases won't work if this header is set
            \flush();
            @\ob_end_flush(); // Send headers and remove output buffering (because that could cause troubles with content-length)
        }
        // Prevent XDebug cutoff
        \ini_set("xdebug.max_nesting_level", 500);
        // Prevent cutoff in regex
        \ini_set("pcre.backtrack_limit", 5000000);

        /* @var $valueConverter ValueToNodeConverter */
        $valueConverter = $this->valueToNodeConverterChain->getFirst();
        $valueNode = $valueConverter->convertToNode($value, $this->valueToNodeConverterChain);

        $callStack = StackTrace::getCurrent();
        if ($this->stackTraceFilter === null)
            $callStack = CurrentDumper::getStackTraceFilterInstance()->filter($callStack);
        else
            $callStack = $this->stackTraceFilter->filter($callStack);
        $trace = $showCallStack ? $valueConverter->convertToNode($callStack, $this->valueToNodeConverterChain) : null;

        $valueConverter->finish($this->valueToNodeConverterChain); // Complete all pending actions and reset afterwards
        $callingLine = $this->printCallingLine ? $callStack->getMostRecentCallFrame()->getCallingLine() : null;

        $plainObjectConverterChain = new ArrayChain($this->plainObjectConverterChain->getElements());
        $linearPlainObjectConverter = new LinearPlainObjectConverter();
        $plainObjectConverterChain->insertElement($linearPlainObjectConverter);

        $id = $callStack->toIdentifier();
        if (isset($this->usedIds[$id]))
            $id .= ++$this->usedIds[$id];
        else
            $this->usedIds[$id] = 0;

        $dumperNode = new JavascriptHtmlDumperNode($id, $valueNode, $linearPlainObjectConverter, $trace, $callingLine);
        $json = $dumperNode->convertToPlainObject($plainObjectConverterChain);

        $this->resourceProvider->provideResources("JavascriptHtmlDumperExternal");

        // Outer div
        echo '<div style="display: none !important;" class="kaemmelot-dump" id="kaemmelot-php-dump-' . $id . '">'; // Style changed in javascript
        // Controls
        echo '<iframe';
        echo ' src="' . $this->resourceProvider->getPackageListingPath("JavascriptHtmlDumperInternalControls") . '"';
        echo ' class="kaemmelot-dump-control"';
        echo ' data-id="' . $id . '"';
        echo '></iframe>';
        // Dumper
        echo '<iframe';
        echo ' src="' . $this->resourceProvider->getPackageListingPath("JavascriptHtmlDumperInternal") . '"';
        echo ' class="kaemmelot-dump"';
        echo ' data-id="' . $id . '"';
        echo ' data-json="' . \htmlspecialchars(\htmlspecialchars(\json_encode($json), ENT_COMPAT | ENT_HTML5, "UTF-8"), ENT_COMPAT | ENT_HTML5, "UTF-8") . '"'; // Double encode
        echo '></iframe>';
        echo '</div>';
    }
}
