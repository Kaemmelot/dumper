<?php

namespace Kaemmelot\Tools\Dumper\Dumpers;

interface Dumper
{
    /**
     * @param mixed $value
     * @param bool  $showStackTrace
     * @return mixed
     */
    function dump($value, $showStackTrace);
}
