<?php

namespace Kaemmelot\Tools\Dumper\ValueToNodeConverters;

use Exception;
use Kaemmelot\Tools\Dumper\Chain;
use Kaemmelot\Tools\Dumper\Nodes\Node;
use Kaemmelot\Tools\Dumper\Nodes\ObjectNode;
use Kaemmelot\Tools\Dumper\Nodes\ObjectProperty;
use ReflectionClass;
use ReflectionProperty;

class ObjectValueToNodeConverter implements ValueToNodeConverter
{
    /**
     * @param mixed $value
     * @param Chain $converterChain
     * @return boolean
     */
    function isComplex($value, Chain $converterChain)
    {
        if (!\is_object($value))
        {
            /* @var $next ValueToNodeConverter */
            $next = $converterChain->getNext($this);

            return $next->isComplex($value, $converterChain);
        }
        else
            return true;
    }

    /**
     * @param mixed $value
     * @param Chain $converterChain
     * @return Node
     */
    public function convertToNode($value, Chain $converterChain)
    {
        if (!\is_object($value))
        {
            /* @var $next ValueToNodeConverter */
            $next = $converterChain->getNext($this);

            return $next->convertToNode($value, $converterChain);
        }

        //$type = Hediet\Types\Type::ofClass(get_class($value)); // TODO save class information once for transmission/lazy loading?
        //$reflector = $type->getReflectionClass();
        $reflector = new ReflectionClass($value);

        $r = $reflector;

        /* @var $properties ReflectionProperty[] */
        $properties = array();
        do
        {
            $properties = \array_merge($properties,
                                       $r->getProperties(
                                           ReflectionProperty::IS_PRIVATE));
            $r = $r->getParentClass();
        } while ($r);

        $properties = \array_merge($properties,
                                   $reflector->getProperties(
                                       ReflectionProperty::IS_PROTECTED
                                       | ReflectionProperty::IS_PUBLIC));

        $objectProperties = array();

        /* @var $converter ValueToNodeConverter */
        $converter = $converterChain->getFirst();

        foreach ($properties as $p)
        {
            $p->setAccessible(true); // the visibility is still the same
            if ($p->isPrivate())
                $visibility = "private";
            else if ($p->isProtected())
                $visibility = "protected";
            else if ($p->isPublic())
                $visibility = "public";

            $propertyValue = $p->getValue($value);

            $ref = $converter->convertToNode($propertyValue, $converterChain);

            $objectProperties[$p->getName()] = new ObjectProperty($p->getName(),
                                                                  $visibility,
                                                                  $p->isStatic(),
                                                                  $p->getDeclaringClass()->getName(),
                                                                  $ref);
        }

        foreach ($value as $name => $property)
        {
            if (!isset($objectProperties[$name]))
            {
                $ref = $converter->convertToNode($property, $converterChain);

                $objectProperties[$name] = new ObjectProperty($name, "public",
                                                              false, null, $ref);
            }
        }

        $outline = null;
        if ($reflector->hasMethod("__getOutline"))
            $outline = $value->__getOutline(); // Allows HTML
        else if ($reflector->hasMethod("__toString")) {
            try { // this could cause problems
                $outline = \htmlspecialchars($value->__toString(), ENT_COMPAT | ENT_HTML5, "UTF-8"); // TODO Helper->escape(maxLen = 100) or better Helper->makeOutline(string) [<span class="string">]
            }
            catch (Exception $e) {}
        }
        
        return new ObjectNode($reflector->getName(), $objectProperties, $outline);
    }

    /**
     * @param Chain $converterChain
     * @return void
     */
    function finish(Chain $converterChain)
    {
        /* @var $next ValueToNodeConverter */
        if (($next = $converterChain->getNext($this)) !== null)
            $next->finish($converterChain);
    }
}
