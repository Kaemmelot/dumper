<?php

namespace Kaemmelot\Tools\Dumper\ValueToNodeConverters;

use Kaemmelot\Tools\Dumper\Chain;
use Kaemmelot\Tools\Dumper\Nodes\AbortDepthNode;
use Kaemmelot\Tools\Dumper\Nodes\Node;
use Kaemmelot\Tools\Dumper\Nodes\ReferenceNode;
use SplObjectStorage;

class DepthFirstSearchValueToNodeConverter implements ValueToNodeConverter
{
    /**
     * @var boolean
     */
    private $ignoreDepthIfLastNode;

    /**
     * @var int
     */
    private $maxDepth;

    /**
     * @var int
     */
    private $depth = 0;

    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var SplObjectStorage
     */
    private $usedObjects;

    /**
     * @param int     $maxDepth
     * @param boolean $ignoreDepthIfLastNode
     */
    public function __construct($maxDepth, $ignoreDepthIfLastNode)
    {
        $this->maxDepth = $maxDepth;
        $this->ignoreDepthIfLastNode = $ignoreDepthIfLastNode;
        $this->usedObjects = new SplObjectStorage();
    }

    /**
     * @param mixed $value
     * @param Chain $converterChain
     * @return boolean
     */
    public function isComplex($value, Chain $converterChain)
    {
        if (($this->maxDepth === 0) || ($this->maxDepth >= $this->depth))
        {
            /* @var $next ValueToNodeConverter */
            $next = $converterChain->getNext($this);

            return $next->isComplex($value, $converterChain);
        }
        else
            return false; // AbortDepth or simple node
    }

    private function initializeUsedObject($object, Node $node = null)
    {
        $this->usedObjects[$object] = array(
            "node"    => $node,
            "waiting" => array()
        );
    }

    private function addWaitingReferenceNode($object, ReferenceNode $node)
    {
        $tmp = $this->usedObjects[$object];
        $tmp["waiting"][] = $node;
        $this->usedObjects[$object] = $tmp;
    }

    /**
     * @param mixed $value
     * @param Chain $converterChain
     * @return Node
     */
    public function convertToNode($value, Chain $converterChain)
    {
        if (\is_object($value) && isset($this->usedObjects[$value]))
        {
            $result = new ReferenceNode();
            if ($this->usedObjects[$value]["node"] === null)
                $this->addWaitingReferenceNode($value, $result);
            else
                $result->setReferencedNodeOnce($this->usedObjects[$value]["node"]);

            return $result;
        }

        $result = null;
        $this->depth++;
        $id = $this->id++;
        /* @var $next ValueToNodeConverter */
        $next = $converterChain->getNext($this);
        $depthOk = ($this->maxDepth === 0) || ($this->maxDepth >= $this->depth) ||
                   ($this->ignoreDepthIfLastNode && !$next->isComplex($value,
                                                                      $converterChain));

        if (\is_object($value) && $depthOk)
        {
            $this->initializeUsedObject($value);

            $result = $next->convertToNode($value, $converterChain);
            $tmp = $this->usedObjects[$value];
            $tmp["node"] = $result;
            /* @var $ref ReferenceNode */
            foreach ($tmp["waiting"] as $ref) $ref->setReferencedNodeOnce($result);
            $tmp["waiting"] = array();
            $this->usedObjects[$value] = $tmp;
        }
        else if ($depthOk)
            $result = $next->convertToNode($value, $converterChain);
        else
            $result = new AbortDepthNode($this->depth);

        if (($result !== null) && ($result->getId() === null))
            $result->setIdOnce($id);

        $this->depth--;

        return $result;
    }

    /**
     * @param Chain $converterChain
     * @return void
     */
    function finish(Chain $converterChain)
    {
        /* @var $next ValueToNodeConverter */
        if (($next = $converterChain->getNext($this)) !== null)
            $next->finish($converterChain);
        $this->depth = 0;
        $this->id = 0;
        $this->usedObjects = new SplObjectStorage();
    }
}
