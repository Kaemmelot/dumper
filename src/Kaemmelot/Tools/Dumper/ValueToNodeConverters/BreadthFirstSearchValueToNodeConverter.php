<?php

namespace Kaemmelot\Tools\Dumper\ValueToNodeConverters;

use Kaemmelot\Tools\Dumper\Chain;
use Kaemmelot\Tools\Dumper\Nodes\AbortDepthNode;
use Kaemmelot\Tools\Dumper\Nodes\Node;
use Kaemmelot\Tools\Dumper\Nodes\ReferenceNode;
use SplObjectStorage;

class BreadthFirstSearchValueToNodeConverter implements ValueToNodeConverter
{
    /**
     * @var boolean
     */
    private $ignoreDepthIfLastNode;

    /**
     * @var int
     */
    private $maxDepth;

    /**
     * @var int
     */
    private $depth = 0;

    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var SplObjectStorage
     */
    private $usedObjects;

    /**
     * @var array
     */
    private $waitList = array();

    /**
     * @var boolean
     */
    private $addToWaitList = false;

    /**
     * @param int     $maxDepth
     * @param boolean $ignoreDepthIfLastNode
     */
    public function __construct($maxDepth, $ignoreDepthIfLastNode)
    {
        $this->maxDepth = $maxDepth;
        $this->ignoreDepthIfLastNode = $ignoreDepthIfLastNode;
        $this->usedObjects = new SplObjectStorage();
    }

    /**
     * @param mixed $value
     * @param Chain $converterChain
     * @return boolean
     */
    public function isComplex($value, Chain $converterChain)
    {
        if (($this->maxDepth === 0) || ($this->maxDepth >= $this->depth))
        {
            /* @var $next ValueToNodeConverter */
            $next = $converterChain->getNext($this);

            return $next->isComplex($value, $converterChain);
        }
        else
            return false; // AbortDepth or simple node
    }

    private function initializeUsedObject($object, Node $node = null)
    {
        $this->usedObjects[$object] = array(
            "node"    => $node,
            "waiting" => array(),
            "id"      => (string) $this->id++
        );
    }

    private function addWaitingReferenceNode($object, ReferenceNode $node)
    {
        $tmp = $this->usedObjects[$object];
        $tmp["waiting"][] = $node;
        $this->usedObjects[$object] = $tmp;
    }

    /**
     * @param ReferenceNode[] $waitingNodes
     * @param Node            $value
     */
    private function updateWaitingReferenceNodes($waitingNodes, $value)
    {
        foreach ($waitingNodes as $node)
            $node->setReferencedNodeOnce($value);
    }

    private function handleWaitList(Chain $converterChain)
    {
        /* @var $next ValueToNodeConverter */
        $next = $converterChain->getNext($this);
        $depth = $this->depth;
        while (\count($this->waitList) !== 0)
        {
            $waitList = $this->waitList;
            $this->waitList = array();
            foreach ($waitList as $entry)
            {
                $value = $entry["value"];
                $this->depth = $entry["depth"];
                $result = $next->convertToNode($value, $converterChain);
                if (\is_object($value) && ($result !== null) && ($result->getId() === null))
                    $result->setIdOnce($this->usedObjects[$value]["id"]);
                else if (($result !== null) && ($result->getId() === null))
                    $result->setIdOnce($entry["id"]);

                if (\is_object($value))
                {
                    $tmp = $this->usedObjects[$value];
                    $this->updateWaitingReferenceNodes($tmp["waiting"], $result);
                    $tmp["waiting"] = array();
                    $tmp["node"] = $result;
                    $this->usedObjects[$value] = $tmp;
                }
                else // Other complex nodes
                    $this->updateWaitingReferenceNodes($entry["waiting"], $result);
            }
        }
        $this->depth = $depth;
    }

    /**
     * @param mixed $value
     * @param Chain $converterChain
     * @return Node
     */
    public function convertToNode($value, Chain $converterChain)
    {
        $result = null;
        $this->depth++;
        /* @var $next ValueToNodeConverter */
        $next = $converterChain->getNext($this);
        $isComplex = $next->isComplex($value, $converterChain);
        $depthOk = ($this->maxDepth === 0) || ($this->maxDepth >= $this->depth) || // depth ok?
                   ($this->ignoreDepthIfLastNode && !$isComplex) || // non-complex allowed?
                   (\is_object($value) && isset($this->usedObjects[$value])); // recursive?

        if ($depthOk && $isComplex && $this->addToWaitList) // search is running
        {
            $result = new ReferenceNode();
            if (\is_object($value) && !isset($this->usedObjects[$value]))
            {
                $this->waitList[] = array("value" => $value, "depth" => $this->depth);
                $this->initializeUsedObject($value);
            }
            else if (!\is_object($value)) // Other complex nodes
                $this->waitList[] = array(
                    "value"   => $value,
                    "waiting" => array($result),
                    "id"      => (string) ($this->id++),
                    "depth"   => $this->depth
                );

            if (\is_object($value) && $this->usedObjects[$value]["node"] !== null)
                $result->setReferencedNodeOnce($this->usedObjects[$value]["node"]);
            else if (\is_object($value))
                $this->addWaitingReferenceNode($value, $result);
        }
        else if ($depthOk && $isComplex && !$this->addToWaitList) // start search
        {
            $this->addToWaitList = true;
            if (\is_object($value))
            {
                $this->initializeUsedObject($value);

                $result = $next->convertToNode($value, $converterChain);

                $tmp = $this->usedObjects[$value];
                /* @var $ref ReferenceNode */
                foreach ($tmp["waiting"] as $ref)
                    $ref->setReferencedNodeOnce($result);
                $tmp["waiting"] = array();
                $tmp["node"] = $result;
                $this->usedObjects[$value] = $tmp;
            }
            else
            {
                $this->waitList[] = array(
                    "value"   => $value,
                    "waiting" => array(),
                    "id"      => "-1", // Should be unused
                    "depth"   => $this->depth
                );
                \end($this->waitList);
                $waitListEntry = \key($this->waitList);

                $result = $next->convertToNode($value, $converterChain);

                $this->updateWaitingReferenceNodes($this->waitList[$waitListEntry]["waiting"],
                                                   $result);

                unset($this->waitList[$waitListEntry]);
            }
            $this->addToWaitList = false;
        }
        else if ($depthOk && !$isComplex)
            $result = $next->convertToNode($value, $converterChain);
        /*else if (\is_object($value)) // !$depthOk
        {
            // TODO allow change of AbortDepthNode on multiple calls (e.g. trace)
        }*/
        else
            $result = new AbortDepthNode($this->depth);

        $needsId = ($result !== null) && ($result->getId() === null);
        if ($isComplex && \is_object($value) && $needsId && !($result instanceof ReferenceNode)
            && !($result instanceof AbortDepthNode)
        )
            $result->setIdOnce($this->usedObjects[$value]["id"]);
        else if ($needsId && !($result instanceof ReferenceNode))
            $result->setIdOnce((string) ($this->id++));

        $this->depth--;

        return $result;
    }

    /**
     * @param Chain $converterChain
     * @return void
     */
    function finish(Chain $converterChain)
    {
        $this->addToWaitList = true;
        $this->handleWaitList($converterChain);
        /* @var $next ValueToNodeConverter */
        if (($next = $converterChain->getNext($this)) !== null)
            $next->finish($converterChain);
        $this->depth = 0;
        $this->id = 0;
        $this->usedObjects = new SplObjectStorage();
        $this->waitList = array();
        $this->addToWaitList = false;
    }
}
