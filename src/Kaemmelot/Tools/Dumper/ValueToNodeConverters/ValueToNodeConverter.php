<?php

namespace Kaemmelot\Tools\Dumper\ValueToNodeConverters;

use Kaemmelot\Tools\Dumper\Chain;
use Kaemmelot\Tools\Dumper\Nodes\Node;

interface ValueToNodeConverter
{
    /**
     * @param mixed $value
     * @param Chain $converterChain
     * @return boolean
     */
    function isComplex($value, Chain $converterChain);

    /**
     * @param mixed $value
     * @param Chain $converterChain
     * @return Node
     */
    function convertToNode($value, Chain $converterChain);

    /**
     * @param Chain $converterChain
     * @return void
     */
    function finish(Chain $converterChain);
}
