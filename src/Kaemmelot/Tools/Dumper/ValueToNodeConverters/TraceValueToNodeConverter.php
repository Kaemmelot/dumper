<?php

namespace Kaemmelot\Tools\Dumper\ValueToNodeConverters;

use Exception;
use Kaemmelot\StackTrace\CallFrames\CallFrame;
use Kaemmelot\StackTrace\CallFrames\ClosureCallFrame;
use Kaemmelot\StackTrace\CallFrames\FunctionCallFrame;
use Kaemmelot\StackTrace\CallFrames\InstanceMethodCallFrame;
use Kaemmelot\StackTrace\CallFrames\MethodCallFrame;
use Kaemmelot\StackTrace\FileSource;
use Kaemmelot\StackTrace\StackTrace;
use Kaemmelot\Tools\Dumper\Chain;
use Kaemmelot\Tools\Dumper\Nodes\CallFrameNode;
use Kaemmelot\Tools\Dumper\Nodes\Node;
use Kaemmelot\Tools\Dumper\Nodes\StackTraceNode;
use ReflectionClass;

class TraceValueToNodeConverter implements ValueToNodeConverter
{
    /**
     * @param mixed $value
     * @param Chain $converterChain
     * @return boolean
     */
    function isComplex($value, Chain $converterChain)
    {
        if (($value instanceof StackTrace) || ($value instanceof CallFrame))
            return true;

        /* @var $next ValueToNodeConverter */
        $next = $converterChain->getNext($this);

        return $next->isComplex($value, $converterChain);
    }

    /**
     * @param mixed $value
     * @param Chain $converterChain
     * @return Node
     * @throws Exception
     */
    public function convertToNode($value, Chain $converterChain)
    {
        if ($value instanceof StackTrace) // TODO add modules
        {
            /* @var $first ValueToNodeConverter */
            $first = $converterChain->getFirst();
            $callFrameNodes = array();
            foreach ($value->getCallFrames() as $callFrame)
                $callFrameNodes[] = $first->convertToNode($callFrame, $converterChain);

            return new StackTraceNode($callFrameNodes);
        }
        else if ($value instanceof CallFrame)
        {
            $type = new ReflectionClass($value);
            $type = $type->getShortName();
            /* @var $first ValueToNodeConverter */
            $first = $converterChain->getFirst();
            $sourceType = $value->hasSource() ? new ReflectionClass($value->getSource()) : null;
            $content = array(
                "arguments"  => $first->convertToNode($value->getArgumentsByName(), $converterChain),
                "sourceLine" => $value->getSourceLine()
            );
            if ($value->hasSource())
            {
                if ($value->getSource() instanceof FileSource)
                    $content["source"] = array(
                        "sourceType" => $value->hasSource() ? $sourceType->getShortName() : null,
                        "path"       => $value->getSource()->getPath(),
                        "content"    => $first->convertToNode($value->getSource()->getContent(), $converterChain));
                else //if ($value->getSource() instanceof EvalSource) // or other
                    $content["source"] = array(
                        "sourceType" => $value->hasSource() ? $sourceType->getShortName() : null,
                        "content"    => $first->convertToNode($value->getSource()->getContent(), $converterChain));
            }
            if ($value instanceof FunctionCallFrame)
            {
                $content["targetFunctionName"] = $value->getTargetFunctionName();
                if ($value instanceof ClosureCallFrame)
                {
                    $content["targetClosureScopeNamespace"] = $value->getTargetClosureScope()->getNamespace();
                    $content["targetClosureScopeClass"] = $value->getTargetClosureScope()->getClass();
                    $content["targetClosureScopeObject"] = ($value->getTargetClosureScope()->getObject() !== null) ?
                        $first->convertToNode($value->getTargetClosureScope()->getObject(), $converterChain) : null;
                    if ($value->hasTargetClosureHandle())
                    {
                        $targetType = new ReflectionClass($value->getTargetClosureHandle()->getSource());
                        if ($value->getTargetClosureHandle()->getSource() instanceof FileSource)
                            $content["targetClosureHandleSource"] = array(
                                "sourceType" => $targetType->getShortName(),
                                "path"       => $value->getTargetClosureHandle()->getSource()->getPath(),
                                "content"    => $first->convertToNode($value->getTargetClosureHandle()
                                                                            ->getSource()
                                                                            ->getContent(), $converterChain));
                        else //if ($value->getTargetClosureHandle()->getSource() instanceof EvalSource) // or other
                            $content["targetClosureHandleSource"] = array(
                                "sourceType" => $targetType->getShortName(),
                                "content"    => $first->convertToNode($value->getTargetClosureHandle()
                                                                            ->getSource()
                                                                            ->getContent(), $converterChain));
                        $content["targetClosureHandleContent"] =
                            $first->convertToNode($value->getTargetClosureHandle()->getContent(), $converterChain);
                    }
                }
            }
            else if ($value instanceof MethodCallFrame)
            {
                $content["targetClassName"] = $value->getTargetClassName();
                $content["targetMethodName"] = $value->getTargetMethodName();
                if ($value instanceof InstanceMethodCallFrame)
                    $content["targetObject"] = $first->convertToNode($value->getTargetObject(), $converterChain);
            }
            else
                throw new Exception("Unknown callFrame type '" . $type . "'");

            return new CallFrameNode($type, $content);
        }
        else
        {
            /* @var $next ValueToNodeConverter */
            $next = $converterChain->getNext($this);

            return $next->convertToNode($value, $converterChain);
        }
    }

    /**
     * @param Chain $converterChain
     * @return void
     */
    function finish(Chain $converterChain)
    {
        /* @var $next ValueToNodeConverter */
        if (($next = $converterChain->getNext($this)) !== null)
            $next->finish($converterChain);
    }
}
