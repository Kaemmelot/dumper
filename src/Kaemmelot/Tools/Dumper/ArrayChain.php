<?php

namespace Kaemmelot\Tools\Dumper;

use Exception;

class ArrayChain implements Chain
{

    private static $id = 0;

    private $elements;

    private $currentKey = null;

    public function __construct(array $elements = array())
    {
        $this->elements = $elements;
        $this->update();
    }

    private function update()
    {
        if ($this->currentKey !== null)
        {
            $key = "__ArrayChain_Next_" . $this->currentKey;

            foreach ($this->elements as $element)
            {
                unset($element->$key);
            }
        }

        $this->currentKey = self::$id++;
        $key = "__ArrayChain_Next_" . $this->currentKey;

        $prev = null;
        foreach ($this->elements as $element)
        {
            if ($prev !== null)
                $prev->$key = $element;
            $prev = $element;
        }
        if ($prev !== null)
            $prev->$key = false; // FIX for php's isset(null) === false
    }

    public function getNext($element)
    {
        $key = "__ArrayChain_Next_" . $this->currentKey;
        if (!isset($element->$key))
            throw new Exception("element is not in the chain");

        return ($element->$key !== false) ? $element->$key : null;
    }

    public function getFirst()
    {
        return reset($this->elements);
    }

    public function getElements()
    {
        return $this->elements;
    }

    public function setElements(array $elements)
    {
        $this->elements = $elements;
        $this->update();
    }

    public function insertElement($element, $idx = 0)
    {
        array_splice($this->elements, $idx, 0, array($element));
        $this->update();
    }

    public function removeElement($element)
    {
        $key = \array_search($element, $this->elements);
        if ($key !== false)
        {
            unset($this->elements[$key]);
            $this->update();

            return true;
        }

        return false;
    }
}
