var gulp              = require('gulp'),
    del               = require('del'),
    filter            = require('gulp-filter'),
    notify            = require('gulp-notify'),
    path              = require('path'),
    rename            = require('gulp-rename'),
    replace           = require('gulp-replace'),
    requirejsOptimize = require('gulp-requirejs-optimize'),
    runSequence       = require('run-sequence'),
    sass              = require('gulp-sass'),
    sourcemaps        = require('gulp-sourcemaps'),
    typescript        = require('gulp-typescript'),
    typings           = require('gulp-typings'),
    util              = require('gulp-util'),
    watch             = require('gulp-watch'),
    notifier          = require('node-notifier'); // For direct logging

gulp.task('clean:typescript', function () {
    var tsProject = typescript.createProject('tsconfig.json'); // TODO own config that changes this value (src/dist)
    return del([path.join(tsProject.options['outDir'], '*.js'), path.join(tsProject.options['outDir'], '*.js.map')]);
});

gulp.task('clean:sass', function () {
    return del(['dist/css/*.css', 'dist/css/*.css.map']);
});

gulp.task('clean:templates', function () {
    return del(['dist/templates/*.tmpl.html']);
});

gulp.task('clean:libs', function () {
    return del(['lib/*']);
});

gulp.task('clean', ['clean:typescript', 'clean:sass', 'clean:templates', 'clean:libs']);

gulp.task('compile:typescript', function () {
    var tsProject = typescript.createProject('tsconfig.json'),
        srcBase = path.resolve(path.dirname(tsProject.configFileName), tsProject.options['rootDir'] ? tsProject.options['rootDir'] : ''), // own implementation because of gulp-typescript bug
        files = tsProject.options['allowJs'] ?
            [path.join(srcBase, '**/*.ts'), path.join(srcBase, '**/*.tsx'), path.join(srcBase, '**/*.js'), path.join(srcBase, '**/*.jsx')]
            : [path.join(srcBase, '**/*.ts'), path.join(srcBase, '**/*.tsx')];

    return gulp.src(files)
        .on('data', function (file) {
            util.log('Compiled', util.colors.magenta(file.relative)); // TODO config
        })
        .pipe(sourcemaps.init())
        .pipe(typescript(tsProject, undefined, typescript.reporter.longReporter()))
        .on('error', function () {
            this.on('finish', function () {
                notifier.notify({ // TODO this is aborted by process.exit(1)
                    message: 'Compiling typescript failed',
                    title: 'PHP-Dumper',
                    icon: path.join(__dirname, 'node_modules/gulp-notify/assets/gulp-error.png')
                });
                process.exit(1);
            });
        })
        .js
        .pipe(sourcemaps.write('.', { // TODO sourcemaps don't seem to work with queries
            includeContent: false,
            sourceMappingURLPrefix: 'index.php?kaemmelot-dumper-resource=' + tsProject.options['outDir'].replace(/\\/g,'/')
        }))
        .pipe(gulp.dest(tsProject.options['outDir']));
});

gulp.task('compress:js', function () {
    var tsProject = typescript.createProject('tsconfig.json'), // TODO own config that changes this value
        destBase = path.resolve(path.dirname(tsProject.configFileName), tsProject.options['outDir'] ? tsProject.options['outDir'] : '');
    return gulp.src([path.join(destBase, 'internal.js'), path.join(destBase, 'external.js'), path.join(destBase, 'internal-controls.js')])
        .pipe(sourcemaps.init()) // { loadMaps:true } ?
        .pipe(requirejsOptimize(function (file) {
            return {
                baseUrl: file.base,
                include: file.relative,
                findNestedDependencies: true,
                skipModuleInsertion: true,
                keepBuildDir: false,
                rawText: { // This will be loaded dynamically
                    'jquery': '',
                    'ResizeSensor': '',
                    'velocity': '',
                    'domReady': ''
                },
                paths: {
                    //'domReady': '../../lib/domReady'
                },
                onBuildWrite: function (moduleName, path, contents) { // executed for every module before compressing it
                    if ((moduleName === 'internal.js') || (moduleName === 'external.js') || (moduleName === 'internal-controls.js'))
                        return contents.replace('define(\'' + moduleName + '\'', 'define(\'' + moduleName.slice(0, -3) + '.min\''); // rename
                    else
                        return contents;
                }
            };
        }))
        .pipe(rename(function (path) {
            path.basename += '.min';
        }))
        .pipe(sourcemaps.write('.', {
            includeContent: false,
            sourceMappingURLPrefix: 'index.php?kaemmelot-dumper-resource=' + tsProject.options['outDir'].replace(/\\/g,'/')
        }))
        .pipe(gulp.dest(tsProject.options['outDir']));
});

gulp.task('finish:external.js', function () {
    var tsProject = typescript.createProject('tsconfig.json'),
        destBase = path.resolve(path.dirname(tsProject.configFileName), tsProject.options['outDir'] ? tsProject.options['outDir'] : '');
    return gulp.src([path.join(destBase, 'external.min.js')])
        .pipe(replace(/define\("/g, 'kaemmelotDefine("'))
        .pipe(gulp.dest(tsProject.options['outDir']));
});

gulp.task('compile:sass', function () {
    return gulp.src([path.relative('.', 'src/sass/*.sass'), path.relative('.', 'src/sass/*.scss')])
        .on('data',function (file) {
            util.log('Compiling', util.colors.magenta(file.relative)); // TODO config
        })
        .pipe(sourcemaps.init())
        .pipe(sass({
            indentWidth: 4,
            sourceComments: true,
            outputStyle: 'expanded' // compressed has an extra task
        }).on('error', function (error) {
            this.on('end', function () {
                notifier.notify({ // TODO this is aborted by process.exit(1)
                    message: 'Compiling sass failed',
                    title: 'PHP-Dumper',
                    icon: path.join(__dirname, 'node_modules/gulp-notify/assets/gulp-error.png')
                });
                process.exit(1);
            });
            sass.logError.call(this, error);
        }))
        .pipe(sourcemaps.write('.', {
            includeContent: false,
            sourceMappingURLPrefix: 'index.php?kaemmelot-dumper-resource=dist/css/'
        }))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('compress:css', function () {
    return gulp.src(['dist/css/*.css', '!dist/css/*.min.css'])
        .on('data',function (file) {
            util.log('Optimizing', util.colors.magenta(file.relative));
        })
        .pipe(sourcemaps.init()) // { loadMaps:true } ?
        .pipe(sass({
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(rename(function (path) {
            path.basename += '.min';
        }))
        .pipe(sourcemaps.write('.', {
            includeContent: false,
            sourceMappingURLPrefix: 'index.php?kaemmelot-dumper-resource=dist/css/'
        }))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('copy:templates', function () {
    return gulp.src(['src/templates/*.tmpl.html'])
        .pipe(gulp.dest('dist/templates'));
});

gulp.task('copy:libs', function () {
    return gulp.src(['node_modules/requirejs/require.js', 'node_modules/jquery/dist/jquery.js', 'node_modules/jquery/dist/jquery.min.js',
        'node_modules/jquery/dist/jquery.min.map', 'node_modules/css-element-queries/src/ResizeSensor.js',
        'node_modules/velocity-animate/velocity.js', 'node_modules/velocity-animate/velocity.min.js',
        'node_modules/requirejs-domready/domReady.js'])
        .pipe(gulp.dest('lib'));
});

gulp.task('compress:libs', function () {
    return gulp.src(['lib/require.js', 'lib/ResizeSensor.js', 'lib/domReady.js'])
        .pipe(sourcemaps.init()) // { loadMaps:true } ?
        .pipe(requirejsOptimize(function (file) {
            return {
                baseUrl: file.base,
                include: file.relative,
                findNestedDependencies: false,
                skipModuleInsertion: true,
                keepBuildDir: false,
                onBuildWrite: function (moduleName, path, contents) { // executed for every module before compressing it
                    if (moduleName === 'domReady.js')
                        return contents.replace('define(\'' + moduleName + '\'', 'kaemmelotDefine(\'' + moduleName.slice(0, -3) + '\''); // rename
                    else if (moduleName === 'require.js')
                        return 'window.kaemmelotOldDefine=window.define;window.kaemmelotOldRequire=window.require;window.kaemmelotOldRequirejs=window.requirejs;' + contents;
                    else
                        return contents;
                }
            };
        }))
        .pipe(rename(function (path) {
            path.basename += '.min';
        }))
        .pipe(sourcemaps.write('.', {
            includeContent: false,
            sourceMappingURLPrefix: 'index.php?kaemmelot-dumper-resource=lib/'
        }))
        .pipe(gulp.dest('lib'));
});

gulp.task('build:typescript', function (callback) {
    runSequence('compile:typescript', 'compress:js', 'finish:external.js', callback); // in order
});

gulp.task('clean-build:typescript', function (callback) {
    runSequence('clean:typescript', 'build:typescript', function () {
        notifier.notify({message:'Clean-Build for Typescript finished', title:'PHP-Dumper'});
        callback();
    }); // in order
});

gulp.task('build:sass', function (callback) {
    runSequence('compile:sass', 'compress:css', callback); // in order
});

gulp.task('clean-build:sass', function (callback) {
    runSequence('clean:sass', 'build:sass', function () {
        gulp.src('!').pipe(notify({message:'Clean-Build for Sass finished', title:'PHP-Dumper'}));
        callback();
    }); // in order
});

gulp.task('build:templates', ['copy:templates']); // TODO minify templates?

gulp.task('clean-build:templates', function (callback) {
    runSequence('clean:templates', 'build:templates', function () {
        gulp.src('!').pipe(notify({message:'Clean-Build for Templates finished', title:'PHP-Dumper'})); // TODO errors?
        callback();
    }); // in order
});

gulp.task('build:libs', function (callback) {
    runSequence('copy:libs', 'compress:libs', callback); // in order
});

gulp.task('clean-build:libs', function (callback) {
    runSequence('clean:libs', 'build:libs', function () {
        gulp.src('!').pipe(notify({message:'Clean-Build for Libs finished', title:'PHP-Dumper'}));
        callback();
    }); // in order
});

gulp.task('build', ['build:typescript', 'build:sass', 'build:templates', 'build:libs'], function () {
    return gulp.src('!').pipe(notify({message:'Build finished', title:'PHP-Dumper'}));
});

gulp.task('clean-build', function (callback) {
    runSequence('clean', 'build', callback); // in order
});

gulp.task('dev-build', function () {
    return watch(['src/ts/*.ts', 'src/ts/*.js', 'src/sass/*.sass', 'src/sass/*.scss', 'src/templates/*.tmpl.html'], function () {
        gulp.start('build');
    });
});

gulp.task('setup', function () { // TODO config/prompt
    return gulp.src("./typings.json")
        .pipe(typings());
});
