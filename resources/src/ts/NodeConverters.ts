﻿/// <reference path="../../typings/index.d.ts" />
import Nodes = require("./Nodes");
import Representations = require("./Representations");
import NodeRepresentations = require("./NodeRepresentations");

export function convertToRepresentation<T extends Nodes.Node>(node: T, nodes: { [id: string]: Nodes.Node }, parent: JQuery,
    parentElement: Element, representationStorage: Representations.RepresentationStateStorage, elementReferences: { [id: string]: JQuery } = {}): Representations.NodeRepresentation<T> {
    if (!(node.getId() in elementReferences))
        elementReferences[node.getId()] = parent;
    var registry: NodeConverterRegistry = new NodeConverterRegistry(nodes);
    registry.registerConverter(new TraceValueNodeConverter(registry, representationStorage)); // TODO config
    registry.registerConverter(new ObjectNodeConverter(registry, representationStorage));
    registry.registerConverter(new PrimitiveNodeConverter());
    registry.registerConverter(new ArrayNodeConverter(registry, representationStorage));
    registry.registerConverter(new UnknownNodeConverter());
    registry.registerConverter(new ReferenceNodeConverter(registry));
    registry.registerConverter(new AbortDepthNodeConverter());
    return registry.convert(node, parentElement, elementReferences);
}

//#region Abstract Converter

export class NodeConverter<T extends Nodes.Node> {
    public isAccepted(node: Nodes.Node): boolean {
        throw "Abstract function not implemented";
    }

    public getId(): string {
        throw "Abstract function not implemented";
    }

    public convert(node: T, parentElement: Element, elementReferences: { [id: string]: JQuery }): Representations.NodeRepresentation<T> {
        throw "Abstract function not implemented";
    }
}

//#endregion

export class NodeConverterRegistry extends NodeConverter<Nodes.Node> {
    private nodeConverters: { [id: string]: NodeConverter<Nodes.Node> } = {};

    constructor(private nodes: { [id: string]: Nodes.Node }) {
        super();
    }

    public isAccepted(node: Nodes.Node): boolean {
        for (var key in this.nodeConverters) {
            if (this.nodeConverters[key].isAccepted(node))
                return true;
        }
        return false;
    }

    public getConverterForNode<T extends Nodes.Node>(node: T): NodeConverter<T> {
        for (var key in this.nodeConverters) {
            if (this.nodeConverters[key].isAccepted(node))
                return <NodeConverter<T>>this.nodeConverters[key];
        }
        throw "Node type '" + node.getType() + "' not supported";
    }

    public registerConverter(converter: NodeConverter<Nodes.Node>): void {
        if (typeof this.nodeConverters[converter.getId()] !== "undefined")
            throw "Converter type already registered";
        this.nodeConverters[converter.getId()] = converter;
    }

    public unregisterConverter(converter: NodeConverter<Nodes.Node>): void {
        if (typeof this.nodeConverters[converter.getId()] === "undefined")
            throw "Converter is not yet registered";
        delete this.nodeConverters[converter.getId()];
    }

    public convert<T extends Nodes.Node>(node: T, parentElement: Element,
        elementReferences: { [id: string]: JQuery } = {},
        representationStorage: Representations.RepresentationStateStorage = null): Representations.NodeRepresentation<T> {
        return this.getConverterForNode<T>(node).convert(node, parentElement, elementReferences);
    }

    public getNode(id: string): Nodes.Node {
        if (!(id in this.nodes))
            throw "Node with id '" + id + "' unknown";
        return this.nodes[id];
    }

    public getId(): string {
        return "NodeConverterReistry";
    }
}

//#region Simple Converter

export class AbortDepthNodeConverter extends NodeConverter<Nodes.AbortDepthNode> {
    public isAccepted(node: Nodes.Node): boolean {
        return node instanceof Nodes.AbortDepthNode;
    }

    public getId(): string {
        return "AbortDepthNodeConverter";
    }

    public convert(node: Nodes.AbortDepthNode, parentElement: Element, elementReferences: { [id: string]: JQuery }): NodeRepresentations.AbortDepthNodeRepresentation {
        return new NodeRepresentations.AbortDepthNodeRepresentation(node, parentElement);
    }
}

export class UnknownNodeConverter extends NodeConverter<Nodes.UnknownNode> {
    public isAccepted(node: Nodes.Node): boolean {
        return node instanceof Nodes.UnknownNode;
    }

    public getId(): string {
        return "UnknownNodeConverter";
    }

    public convert(node: Nodes.UnknownNode, parentElement: Element, elementReferences: { [id: string]: JQuery }): NodeRepresentations.UnknownNodeRepresentation {
        return new NodeRepresentations.UnknownNodeRepresentation(node, parentElement);
    }
}

export class PrimitiveNodeConverter extends NodeConverter<Nodes.PrimitiveNode> {
    public isAccepted(node: Nodes.Node): boolean {
        return node instanceof Nodes.PrimitiveNode;
    }

    public getId(): string {
        return "PrimitiveNodeConverter";
    }

    public convert(node: Nodes.PrimitiveNode, parentElement: Element, elementReferences: { [id: string]: JQuery }): NodeRepresentations.PrimitiveNodeRepresentation {
        return new NodeRepresentations.PrimitiveNodeRepresentation(node, parentElement);
    }
}

//#endregion

//#region Complex Converter

export class ReferenceNodeConverter extends NodeConverter<Nodes.Node> {
    constructor(private nodeConverterRegistry: NodeConverterRegistry) {
        super();
    }

    public isAccepted(node: Nodes.Node): boolean {
        return node instanceof Nodes.ReferenceNode;
    }

    public getId(): string {
        return "ReferenceNodeConverter";
    }

    public convert(node: Nodes.ReferenceNode, parentElement: Element, elementReferences: { [id: string]: JQuery }): Representations.NodeRepresentation<Nodes.Node> {
        var referencedNode = this.nodeConverterRegistry.getNode(node.getReferencedId());
        if (referencedNode !== null)
            return this.nodeConverterRegistry.convert(referencedNode, parentElement, elementReferences);
        else
            return null;
    }
}

export class ArrayNodeConverter extends NodeConverter<Nodes.ArrayNode> {
    constructor(private nodeConverterRegistry: NodeConverterRegistry, private representationStorage: Representations.RepresentationStateStorage) {
        super();
    }

    public isAccepted(node: Nodes.Node): boolean {
        return node instanceof Nodes.ArrayNode;
    }

    public getId(): string {
        return "ArrayNodeConverter";
    }

    public convert(node: Nodes.ArrayNode, parentElement: Element, elementReferences: { [id: string]: JQuery }): NodeRepresentations.ArrayNodeRepresentation {
        return new NodeRepresentations.ArrayNodeRepresentation(node, parentElement, elementReferences, this.nodeConverterRegistry, this.representationStorage);
    }
}

export class ObjectNodeConverter extends NodeConverter<Nodes.ObjectNode> {
    constructor(private nodeConverterRegistry: NodeConverterRegistry, private representationStorage: Representations.RepresentationStateStorage) {
        super();
    }

    public isAccepted(node: Nodes.Node): boolean {
        return node instanceof Nodes.ObjectNode;
    }

    public getId(): string {
        return "ObjectNodeConverter";
    }

    public convert(node: Nodes.ObjectNode, parentElement: Element, elementReferences: { [id: string]: JQuery }): NodeRepresentations.ObjectNodeRepresentation {
        return new NodeRepresentations.ObjectNodeRepresentation(node, parentElement, elementReferences, this.nodeConverterRegistry, this.representationStorage);
    }
}

export class TraceValueNodeConverter extends NodeConverter<Nodes.ComplexNode> {
    constructor(private nodeConverterRegistry: NodeConverterRegistry, private representationStorage: Representations.RepresentationStateStorage) {
        super();
    }

    public isAccepted(node: Nodes.Node): boolean {
        return (node instanceof Nodes.StackTraceNode) || (node instanceof Nodes.CallFrameNode);
    }

    public getId(): string {
        return "TraceValueNodeConverter";
    }

    public convert(node: Nodes.ComplexNode, parentElement: Element, elementReferences: { [id: string]: JQuery }): Representations.NodeRepresentation<Nodes.ComplexNode> {
        if (node instanceof Nodes.StackTraceNode)
            return new NodeRepresentations.StackTraceNodeRepresentation(node, parentElement, elementReferences, this.nodeConverterRegistry, this.representationStorage);
        else if (node instanceof Nodes.ClosureCallFrameNode)
            return new NodeRepresentations.ClosureCallFrameNodeRepresentation(node, parentElement, elementReferences, this.nodeConverterRegistry, this.representationStorage);
        else if (node instanceof Nodes.FunctionCallFrameNode)
            return new NodeRepresentations.FunctionCallFrameNodeRepresentation(node, parentElement, elementReferences, this.nodeConverterRegistry, this.representationStorage);
        else if (node instanceof Nodes.StaticMethodCallFrameNode)
            return new NodeRepresentations.StaticMethodCallFrameNodeRepresentation(node, parentElement, elementReferences, this.nodeConverterRegistry, this.representationStorage);
        else if (node instanceof Nodes.InstanceMethodCallFrameNode)
            return new NodeRepresentations.InstanceMethodCallFrameNodeRepresentation(node, parentElement, elementReferences, this.nodeConverterRegistry, this.representationStorage);
        else
            throw "node not supported";
    }
}

//#endregion
