﻿/// <reference path="../../typings/index.d.ts" />

interface ResizeSensorStatic {
    new (element: Element | Element[] | JQuery, callback: () => void): ResizeSensorInstance; // constructor
    detach(element: Element): void;
}

interface ResizeSensorInstance {
    detach(): void;
}

declare var ResizeSensor: ResizeSensorStatic;

declare module "ResizeSensor" {
    export = ResizeSensor;
}
