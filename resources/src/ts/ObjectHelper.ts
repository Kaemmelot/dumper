﻿
export function map<TValue, TResult>(obj: { [name: string]: TValue }, mapFunc: (value: TValue, key: string) => TResult): { [name: string]: TResult } {

    var result: { [name: string]: TResult } = {};
    for (var key in obj) {
        var value = obj[key];
        result[key] = mapFunc(value, key);
    }
    return result;
}

export function clone<T>(obj: T): T {
    return Object.create(obj);
}

export function getPropertyValues<TValue>(obj: { [name: string]: TValue }): TValue[] {
    return Object.keys(obj).map(function (key) {
        return obj[key];
    });
}
