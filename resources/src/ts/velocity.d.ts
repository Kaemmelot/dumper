﻿
declare class velocityStatic {
    debug: number;
}

declare module "velocity" {
    export = velocity;
}

declare var velocity: velocityStatic;
