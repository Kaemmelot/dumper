﻿import $ = require("jquery");
import Nodes = require("./Nodes");
import Representations = require("./Representations");
import TabRepresentations = require("./TabRepresentations");
// indirect velocity import


// FIX for typescript/javascript not supporting interfaces in instanceof
export function isComplexRepresentation(object: any) {
    return object instanceof TabRepresentations.TabbedNodeRepresentation
        || object instanceof TabRepresentations.ComplexTabRepresentation
        || object instanceof Representations.ComplexNodeRepresentation;
}

export function getOutlineForNamespacedType(type: string, cssClass: string = null): string {
    var i = type.lastIndexOf("\\");
    if (i >= 0)
        return "<span class=\"" + ((cssClass !== null) ? cssClass : "type") + "\"><span class=\"mouseoverGreyed\">" + type.slice(0, i + 1)
            + "</span>" + type.substr(i + 1) + "</span>";
    else
        return "<span class=\"" + ((cssClass !== null) ? cssClass : "type") + "\">" + type + "</span>";
}

export function getOutlineForSource(source?: Nodes.Source, sourceLine?: number): string {
    var result = "<span class=\"calledFrom\"> called from ";
    if (source !== null) {
        var sourceType = source.getSourceType().charAt(0).toLocaleLowerCase() + source.getSourceType().slice(1, -6);
        if (sourceLine !== null)
            result += "<span class=\"sourceline " + sourceType + "\">";
        if (source instanceof Nodes.FileSource) {
            var path = source.getPath();
            var lastSep = Math.max(path.lastIndexOf("\\"), path.lastIndexOf("/"));
            result += "<span class=\"filepath " + sourceType + "\"><span class=\"mouseoverGreyed\">" + path.slice(0, lastSep + 1) + "</span>" + path.substr(lastSep + 1) + "</span>";
        } else {
            result += "<span class=\"" + sourceType + "\">" + sourceType + "</span>";
        }
        if (sourceLine !== null)
            result += ":" + sourceLine + "</span>";

    } else
        result += "<span class=\"unknownSource\">*unknown*</span>";
    return result + "</span>";
}

// Trim every textnode but ignore non-breakable spaces
export function trimHtml(element: Element): void {
    $(element).contents().filter(function () {
        return this.nodeType == Node.TEXT_NODE;
    }).each((i, el) => {
        var newValue = el.nodeValue.replace(/^[^\S\u00A0]+|[^\S\u00A0]+$/g, '');
        if (newValue !== "") el.nodeValue = newValue;
        else el.parentNode.removeChild(el);
    });
}

export function switchStateClick(event: JQueryEventObject): boolean {
    var data = <{ representation: Representations.ComplexRepresentation; id: string | number }>event.data;
    var _this = data.representation;
    _this.switch(data.id, event.ctrlKey);

    if (_this.getRepresentationStorage() !== null)
        _this.getRepresentationStorage().saveCurrentState();
    return false;
}

export function resetStateClick(event: JQueryEventObject): boolean {
    var data = <{ representation: Representations.ComplexRepresentation; id: string | number }>event.data;
    var _this = data.representation;
    _this.reset(data.id);
    _this.open(data.id);

    if (_this.getRepresentationStorage() !== null)
        _this.getRepresentationStorage().saveCurrentState();
    return false;
}

export function scrollToOriginalEvent(event: JQueryEventObject): boolean {
    // scroll this window or outer window
    var container = window.document.body,
        body = window.document.body;
    if (window.iframe) {
        container = window.frameElement.parentElement;
        body = window.containerWindow.document.body;
    }
    var rStart = 98,
        gStart = 147,
        bStart = 211;
    $(container).velocity("scroll", {
        duration: 400,
        offset: (<JQuery>event.data).offset().top - body.scrollTop,
        container: $(body),
        complete: () => {
            (<JQuery>event.data).css("background-color", "rgb(" + rStart + "," + gStart + "," + bStart + ")")
                .velocity({ backgroundColorRed: "255", backgroundColorGreen: "255", backgroundColorBlue: "255" }, {
                duration: 800,
                complete: () => {
                    (<JQuery>event.data).css("background-color", ""); // Remove css after animation
                }
            });
        }
    });
    
    return false;
};

export function show(element: JQuery, immidiate: boolean = false, finishCallback: () => void = null) {
    element.velocity("finishAll", true)
        .velocity("slideDown", {
            duration: immidiate ? 0 : 400,
            complete: !immidiate ? finishCallback : null
        });
    if (immidiate && (finishCallback !== null)) // prevent delay
        finishCallback();
}

export function hide(element: JQuery, immidiate: boolean = false, emptyElement: boolean = false, finishCallback: () => void = null) {
    element.velocity("finishAll", true);
    if (emptyElement)
        element.empty(); // empty // TODO empty afterwards?
    element.css("height", "") // and remove old animation height
        .velocity("slideUp", {
            duration: immidiate ? 0 : 400, // TODO 0 gets changed to 1 and is done async. therefore animations jump sometimes with multiple tabs for example
            complete: !immidiate ? finishCallback : null
        });
    if (immidiate && (finishCallback !== null)) // prevent delay
        finishCallback();
}

export function selectElement(event: JQueryEventObject) {
    /*if(document.selection) { // IE?
        document.selection.empty();
        var range = document.body.createTextRange();
        range.moveToElementText($(event.currentTarget)[0]);
        range.select();
    } else*/
    if (window.getSelection) {
        if (window.getSelection().empty) {  // Chrome
            window.getSelection().empty();
        } else if (window.getSelection().removeAllRanges) {  // Firefox
            window.getSelection().removeAllRanges();
        }
        var range = document.createRange();
        range.selectNode($(event.currentTarget)[0]);
        window.getSelection().addRange(range);
    }
}
