﻿/// <reference path="../../typings/index.d.ts" />
import $ = require("jquery");
import ObjectHelper = require("./ObjectHelper");
import Helpers = require("./Helpers");
import Template = require("./Template");
import Nodes = require("./Nodes");
import NodeConverters = require("./NodeConverters");
import Representations = require("./Representations");
import TabRepresentations = require("./TabRepresentations");


//#region Abstract Representations

export class CallFrameNodeRepresentation<T extends Nodes.CallFrameNode> extends TabRepresentations.TabbedNodeRepresentation<T> {
    constructor(node: T, parentElement: Element, elementReferences: { [id: string]: JQuery },
        nodeConverterRegistry: NodeConverters.NodeConverterRegistry, representationStorage: Representations.RepresentationStateStorage = null) {
        super(node, parentElement, elementReferences, nodeConverterRegistry, representationStorage);
        var parent = $(parentElement);
        parent.addClass("callFrame");
        this.type = "CallFrameNode";
    }
}

//#endregion

//#region Simple Representations

export class AbortDepthNodeRepresentation extends Representations.NodeRepresentation<Nodes.AbortDepthNode> {
    constructor(node: Nodes.AbortDepthNode, parentElement: Element) {
        super(node, parentElement);
        // get and render template
        var tmpl = Template.get("AbortDepth");
        var data = { node: node };
        var html = tmpl.render(data);
        // add to parent
        var parent = $(parentElement);
        parent.addClass("abortDepth");
        parent.html(html);
        Helpers.trimHtml(parentElement);
    }
}

export class UnknownNodeRepresentation extends Representations.NodeRepresentation<Nodes.UnknownNode> {
    constructor(node: Nodes.UnknownNode, parentElement: Element) {
        super(node, parentElement);
        // get and render template
        var tmpl = Template.get("Unknown");
        var data = { node: node };
        var html = tmpl.render(data);
        // add to parent
        var parent = $(parentElement);
        parent.addClass("unknown");
        parent.html(html);
        Helpers.trimHtml(parentElement);
    }
}

export class PrimitiveNodeRepresentation extends Representations.NodeRepresentation<Nodes.PrimitiveNode> {
    public static get shortForm(): boolean { return true; } // TODO config

    constructor(node: Nodes.PrimitiveNode, parentElement: Element) {
        super(node, parentElement);
        // get and render template
        var tmpl = Template.get("Primitive");
        var data = { node: node, shortForm: PrimitiveNodeRepresentation.shortForm };
        var html = tmpl.render(data);
        // add to parent
        var parent = $(parentElement);
        parent.addClass("primitive");
        parent.html(html);
        Helpers.trimHtml(parentElement);
        Helpers.trimHtml(parent.children("span.content").get(0));
    }
}

//#endregion

//#region Complex Representations

export class ArrayNodeRepresentation extends TabRepresentations.TabbedNodeRepresentation<Nodes.ArrayNode> {
    constructor(node: Nodes.ArrayNode, parentElement: Element, elementReferences: { [id: string]: JQuery },
        nodeConverterRegistry: NodeConverters.NodeConverterRegistry, representationStorage: Representations.RepresentationStateStorage = null) {
        super(node, parentElement, elementReferences, nodeConverterRegistry, representationStorage);
        var parent = $(parentElement);
        parent.addClass("array");
        this.type = "ArrayNode";
    }
}

export class ObjectNodeRepresentation extends TabRepresentations.TabbedNodeRepresentation<Nodes.ObjectNode> {
    constructor(node: Nodes.ObjectNode, parentElement: Element, elementReferences: { [id: string]: JQuery },
        nodeConverterRegistry: NodeConverters.NodeConverterRegistry, representationStorage: Representations.RepresentationStateStorage = null) {
        super(node, parentElement, elementReferences, nodeConverterRegistry, representationStorage);
        var parent = $(parentElement);
        parent.addClass("object");
        this.type = "ObjectNode";
    }
}

//#region TraceValue Representations

export class StackTraceNodeRepresentation extends Representations.ComplexNodeRepresentation<Nodes.StackTraceNode> {
    private elements: {
        title: JQuery; definition: JQuery; nav: JQuery; open: boolean; representation: Representations.ComplexNodeRepresentation<Nodes.CallFrameNode>
    }[] = [];

    constructor(node: Nodes.StackTraceNode, parentElement: Element, private elementReferences: { [id: string]: JQuery },
        private nodeConverterRegistry: NodeConverters.NodeConverterRegistry, representationStorage: Representations.RepresentationStateStorage) {
        super(node, parentElement, representationStorage);
        // get and render template
        var tmpl = Template.get("StackTrace");
        var data = { elements: node.getElements() };//.slice(1) }; // TODO Do not show the first? -> better outline? -> not for trace()?
        var html = tmpl.render(data);
        // add to parent
        var parent = $(parentElement);
        parent.addClass("stackTrace");
        parent.html(html);
        Helpers.trimHtml(parentElement);
        var titles = parent.children("dl").children("dt");
        titles.each((i, el) => Helpers.trimHtml(el));
        var definitions = parent.children("dl").children("dd");
        definitions.empty();
        for (var key in data.elements) {
            var k = +key; // convert to number
            var element = data.elements[k];
            var nav = titles.eq(k).children("nav").eq(0);
            nav.click({ representation: this, id: k }, Helpers.switchStateClick);
            nav.dblclick({ representation: this, id: k }, Helpers.resetStateClick);
            if (element.isEmpty())
                nav.addClass("empty");
            this.elements[k] = {
                title: titles.eq(k).children("outline").eq(0), definition: definitions.eq(k),
                nav: nav, open: false,
                representation: null
            };
        }
    }

    public open(id: string | number, untilLoop: boolean = false): Representations.NodeRepresentation<Nodes.CallFrameNode> {
        if ((typeof id !== "number") || (this.elements[id] === undefined) || (untilLoop && (typeof this.elementReferences[this.node.getElements()[id].getId()] !== "undefined"))) {
            return null;
        } else {
            this.elements[id].nav.addClass("open");
            if (this.elements[id].representation === null) {
                var node: Nodes.CallFrameNode = this.node.getElements()[id];
                var newReferences = ObjectHelper.clone(this.elementReferences);
                newReferences[node.getId()] = this.elements[id].title.add(this.elements[id].definition);
                this.elements[id].representation = <Representations.ComplexNodeRepresentation<Nodes.CallFrameNode>>this.nodeConverterRegistry.convert(
                    node, this.elements[id].definition[0], newReferences, this.representationStorage);
            }
            if (!this.elements[id].open) {
                Helpers.show(this.elements[id].definition);
                this.elements[id].open = true;
            }
            if (untilLoop) // Open all subelements (i.e. CallFrames)
                this.node.getElements()[id].getElements().forEach((node, index, array) => { this.elements[id].representation.open(index, true); }, this);
            return this.elements[id].representation;
        }
    }

    public close(id: string | number, immidiate: boolean = false): boolean {
        if ((typeof id !== "number") || (typeof this.elements[id] === "undefined")) {
            return false;
        } else {
            this.elements[id].nav.removeClass("open");
            Helpers.hide(this.elements[id].definition, immidiate);
            this.elements[id].open = false;
            return true;
        }
    }

    public switch(id: string | number, complete: boolean = false): void {
        if ((typeof id !== "number") || (typeof this.elements[id] === "undefined")) {
            return;
        } else {
            if (!this.elements[id].open)
                this.open(id, complete);
            else if (complete)
                this.reset(id);
            else
                this.close(id);
        }
    }

    public reset(id: string | number): void {
        if ((typeof id === "number") && (typeof this.elements[id] !== "undefined")) {
            this.elements[id].nav.removeClass("open");
            Helpers.hide(this.elements[id].definition, false, true);
            this.elements[id].representation = null;
            this.elements[id].open = false;
        }
    }

    public getState(): { type: string; elements: { id: number; open: boolean; nodeType: string; criteria: string; state: any }[] } {
        var elements = this.elements.map((element, id) => {
            if (element.representation === null)
                return null;
            var node = this.node.getElements()[id];
            var state: any = null;
            if (element.representation !== null)
                state = (<Representations.ComplexRepresentation><any>element.representation).getState();
            return { id: id, open: element.open, nodeType: node.getNodeType(), criteria: node.getCriteriaString(), state: state };
        });
        return { type: "StackTrace", elements: elements };
    }

    public setState(state: { type: string; elements: { id: number; open: boolean; nodeType: string; criteria: string; state: any }[] }): void {
        if ((state.type !== "StackTrace") || (this.elements.length !== state.elements.length)) {
            this.elements.forEach((element, id) => this.reset(id));
        } else {
            // change state
            this.elements.forEach((element, id) => {
                var node = this.node.getElements()[id];
                if ((typeof state.elements[id] !== "undefined") && (state.elements[id] !== null) &&
                    (state.elements[id].nodeType === node.getNodeType()) && (state.elements[id].criteria === node.getCriteriaString())) {
                    this.open(id);
                    if (!state.elements[id].open) this.close(id, true);
                    if ((element.representation !== null) && (state.elements[id].state !== null))
                        (<Representations.ComplexRepresentation><any>element.representation).setState(state.elements[id].state);
                } else {
                    this.reset(id);
                }
            });
        }
    }

    public isOpen(id: string | number): boolean {
        return (typeof id === "number") && (typeof this.elements[id] !== "undefined") && this.elements[id].open;
    }
}

export class FunctionCallFrameNodeRepresentation extends CallFrameNodeRepresentation<Nodes.FunctionCallFrameNode> {
    constructor(node: Nodes.FunctionCallFrameNode, parentElement: Element, elementReferences: { [id: string]: JQuery },
        nodeConverterRegistry: NodeConverters.NodeConverterRegistry, representationStorage: Representations.RepresentationStateStorage = null) {
        super(node, parentElement, elementReferences, nodeConverterRegistry, representationStorage);
        var parent = $(parentElement);
        parent.addClass("functionCallFrame");
        this.type = "FunctionCallFrameNode";
    }
}

export class ClosureCallFrameNodeRepresentation extends CallFrameNodeRepresentation<Nodes.ClosureCallFrameNode> {
    constructor(node: Nodes.ClosureCallFrameNode, parentElement: Element, elementReferences: { [id: string]: JQuery },
        nodeConverterRegistry: NodeConverters.NodeConverterRegistry, representationStorage: Representations.RepresentationStateStorage = null) {
        super(node, parentElement, elementReferences, nodeConverterRegistry, representationStorage);
        var parent = $(parentElement);
        parent.addClass("closureCallFrame");
        this.type = "ClosureCallFrameNode";
    }
}

export class StaticMethodCallFrameNodeRepresentation extends CallFrameNodeRepresentation<Nodes.StaticMethodCallFrameNode> {
    constructor(node: Nodes.StaticMethodCallFrameNode, parentElement: Element, elementReferences: { [id: string]: JQuery },
        nodeConverterRegistry: NodeConverters.NodeConverterRegistry, representationStorage: Representations.RepresentationStateStorage = null) {
        super(node, parentElement, elementReferences, nodeConverterRegistry, representationStorage);
        var parent = $(parentElement);
        parent.addClass("staticMethodCallFrame");
        this.type = "StaticMethodCallFrameNode";
    }
}

export class InstanceMethodCallFrameNodeRepresentation extends CallFrameNodeRepresentation<Nodes.InstanceMethodCallFrameNode> {
    constructor(node: Nodes.InstanceMethodCallFrameNode, parentElement: Element, elementReferences: { [id: string]: JQuery },
        nodeConverterRegistry: NodeConverters.NodeConverterRegistry, representationStorage: Representations.RepresentationStateStorage = null) {
        super(node, parentElement, elementReferences, nodeConverterRegistry, representationStorage);
        var parent = $(parentElement);
        parent.addClass("instanceMethodCallFrame");
        this.type = "InstanceMethodCallFrameNode";
    }
}

//#endregion
//#endregion
