﻿/// <reference path="../../typings/index.d.ts" />
import $ = require("jquery");
import ObjectHelper = require("./ObjectHelper");
import Helpers = require("./Helpers");
import Template = require("./Template");
import Nodes = require("./Nodes");
import NodeConverters = require("./NodeConverters");
import Representations = require("./Representations");


export function getTabHeader(node: Nodes.ComplexNode): { order: string[], tabHeader: { [id: string]: string } } {
    var result: { order: string[], tabHeader: { [id: string]: string } } = { order: [], tabHeader: {} };

    if (node instanceof Nodes.ArrayNode) {
        result.order.push("elements");
        result.tabHeader["elements"] = "Elements (" + node.getElements().length + ")";
    } else if (node instanceof Nodes.ObjectNode) {
        result.order.push("properties");
        result.tabHeader["properties"] = "Properties (" + node.getElements().length + ")";
    } else if (node instanceof Nodes.CallFrameNode) {
        result.order.push("arguments");
        result.tabHeader["arguments"] = "Arguments (" + node.getElements().length + ")";
        if (node.getSource() !== null) {
            result.order.push("source");
            result.tabHeader["source"] = "Source (" + node.getSource().getSourceType().slice(0, -6) + ")";
        }
        if ((node instanceof Nodes.ClosureCallFrameNode) && (node.getTargetClosureHandle() !== null)) {
            result.order.push("closureHandle");
            result.tabHeader["closureHandle"] = "Closure";
        }
        if ((node instanceof Nodes.ClosureCallFrameNode) && (node.getTargetClosureScopeObject() !== null)) {
            result.order.push("closureObject");
            result.tabHeader["closureObject"] = "Bound Object (" + node.getElements().length + ")";
        }
    }
    return result;
}

export function createTab<T extends Nodes.ComplexNode>(tabbedNodeRepresentation: TabbedNodeRepresentation<Nodes.ComplexNode>,
    tabContent: Element, id: string): TabRepresentation<T> {
    var result: TabRepresentation<any> = null;
    $(tabContent).empty();
    Helpers.hide($(tabContent), true);

    var node = tabbedNodeRepresentation.getNode();
    if (node instanceof Nodes.ArrayNode) {
        if (id === "elements") {
            result = new ArrayElementsTabRepresentation(node, tabbedNodeRepresentation.getRepresentationStorage(), tabContent,
                tabbedNodeRepresentation.getElementReferences(), tabbedNodeRepresentation.getNodeConverterRegistry());
        }
    } else if (node instanceof Nodes.ObjectNode) {
        if (id === "properties") {
            result = new ObjectPropertiesTabRepresentation(node, tabbedNodeRepresentation.getRepresentationStorage(), tabContent,
                tabbedNodeRepresentation.getElementReferences(), tabbedNodeRepresentation.getNodeConverterRegistry());
        }
    } else if (node instanceof Nodes.CallFrameNode) {
        if (id === "arguments") {
            result = new ArrayElementsTabRepresentation(node.getArgs(), tabbedNodeRepresentation.getRepresentationStorage(), tabContent,
                tabbedNodeRepresentation.getElementReferences(), tabbedNodeRepresentation.getNodeConverterRegistry());
        } else if ((id === "source") && (node.getSource() !== null)) {
            result = new SourceTabRepresentation(node, tabbedNodeRepresentation.getRepresentationStorage(), tabContent,
                tabbedNodeRepresentation.getElementReferences(), tabbedNodeRepresentation.getNodeConverterRegistry());
        } else if ((id === "closureHandle") && (node instanceof Nodes.ClosureCallFrameNode) && (node.getTargetClosureHandle() !== null)) {
            result = new ClosureHandleTabRepresentation(node, tabbedNodeRepresentation.getRepresentationStorage(), tabContent,
                tabbedNodeRepresentation.getElementReferences(), tabbedNodeRepresentation.getNodeConverterRegistry());
        } else if ((id === "closureObject") && (node instanceof Nodes.ClosureCallFrameNode) && (node.getTargetClosureScopeObject() !== null)) {
            result = new ObjectPropertiesTabRepresentation(node.getTargetClosureScopeObject(), tabbedNodeRepresentation.getRepresentationStorage(), tabContent,
                tabbedNodeRepresentation.getElementReferences(), tabbedNodeRepresentation.getNodeConverterRegistry());
        }
    }

    return result;
}

//#region Abstract Representations

export class TabbedNodeRepresentation<T extends Nodes.ComplexNode> extends Representations.NodeRepresentation<T> implements Representations.ComplexRepresentation {
    protected tabs: TabRepresentation<T>[] = [];
    protected tabHeaderNames: { order: string[], tabHeader: { [id: string]: string } };
    protected activeTab: number = null;
    protected tabHeader: JQuery[] = [];
    protected tabContents: Element[] = [];
    protected type = "TabbedNode";

    constructor(node: T, parentElement: Element, protected elementReferences: { [id: string]: JQuery },
        protected nodeConverterRegistry: NodeConverters.NodeConverterRegistry, representationStorage: Representations.RepresentationStateStorage = null) {
        super(node, parentElement, representationStorage);
        // get and render template
        var tmpl = Template.get("TabList");
        this.tabHeaderNames = getTabHeader(node);
        var data = { tabs: this.tabHeaderNames };
        var html = tmpl.render(data);
        // add to parent
        var parent = $(parentElement);
        Helpers.hide(parent, true);
        parent.html(html);
        Helpers.trimHtml(parentElement);
        // add content to tabs
        parent.children("ul.header").children("li").each((i, el) => this.tabHeader[i] = $(el));
        parent.children("ul.content").children("li").each((i, el) => this.tabContents[i] = el);
        for (var i = 0; i < this.tabHeaderNames.order.length; i++) {
            this.tabs[i] = createTab<T>(this, this.tabContents[i], this.tabHeaderNames.order[i]);
            // set onclick & ondblclick
            this.tabHeader[i].click({ representation: this, id: i }, Helpers.switchStateClick);
            this.tabHeader[i].dblclick({ representation: this, id: i }, Helpers.resetStateClick);
        }
        Helpers.show(parent);
        Helpers.show($(this.tabContents[0]));
        this.tabHeader[0].addClass("active");
        this.activeTab = 0;
    }

    public getTabs(): TabRepresentation<T>[] {
        return this.tabs;
    }

    public getElementReferences(): { [id: string]: JQuery } {
        return this.elementReferences;
    }

    public getNodeConverterRegistry(): NodeConverters.NodeConverterRegistry {
        return this.nodeConverterRegistry;
    }

    public open(id: string | number, untilLoop: boolean = false): TabRepresentation<T> {
        if ((typeof id === "number") && (typeof this.tabs[id] !== "undefined")) {
            if (this.activeTab !== id) {
                if (this.activeTab !== null)
                    this.close(this.activeTab, true);
                Helpers.show($(this.tabContents[id]));
                this.tabHeader[id].addClass("active");
                this.activeTab = id;
            }
            if (untilLoop)
                (<Nodes.ComplexNode>this.tabs[id].getNode()).getElements()
                    .forEach((element, index, array) => {
                        var value = (<Nodes.ComplexNode>this.tabs[id].getNode()).getElements()[index].getValue();
                        if ((value instanceof Nodes.ComplexNode) && !value.isEmpty())
                            (<ComplexTabRepresentation<T>>this.tabs[id]).open(index, true);
                    }, this);
            return this.tabs[id];
        }
        else
            return null;
    }

    public close(id: string | number, immidiate: boolean = false): boolean {
        if ((typeof id === "number") && (typeof this.tabs[id] !== "undefined") && (this.activeTab === id)) {
            Helpers.hide($(this.tabContents[id]), immidiate);
            this.tabHeader[id].removeClass("active");
            this.activeTab = null;
            return true;
        }
        else
            return false;
    }

    public switch(id: string | number, complete: boolean = false): void {
        if (typeof id !== "number")
            return;

        if ((this.activeTab === id) && !complete)
            this.close(id);
        else if (this.activeTab === id)
            this.reset(id);
        else
            this.open(id, complete);
    }

    public reset(id: string | number): void {
        if (typeof id === "number") {
            if (this.activeTab !== null)
                this.close(this.activeTab, true);
            this.tabs[id] = createTab<T>(this, this.tabContents[id], this.tabHeaderNames.order[id]);
            this.open(id);
        }
    }

    public getState(): { type: string; tabs: any[]; active: any } {
        var result: { type: string; tabs: any[]; active: any } = { type: this.type, tabs: [], active: this.activeTab };
        this.tabs.forEach(function (tab, id) {
            if (Helpers.isComplexRepresentation(tab)) {
                result.tabs[id] = (<Representations.ComplexRepresentation>(<any>tab)).getState();
            }
        });
        return result;
    }

    public setState(state: { type: string; tabs: any[]; active: any }): void {
        if (state.type !== this.type) {
            this.tabs.forEach((tab, i) => this.reset(i));
            for (var first in this.tabs) break;
            this.open(+first); // convert to number if possible
        } else {
            // change state
            state.tabs.forEach((tab, id) => {
                if ((typeof this.tabs[id] !== "undefined") && Helpers.isComplexRepresentation(this.tabs[id])) {
                    (<Representations.ComplexRepresentation>(<any>this.tabs[id])).setState(tab);
                }
            });
            if (state.active === null) {
                this.close(this.activeTab, true);
            } else if (!this.open(state.active)) {
                for (var first in this.tabs) break;
                this.open(+first); // convert to number if possible
            }
        }
    }

    public isOpen(id: string | number) {
        return this.activeTab === id;
    }
}

export class TabRepresentation<T extends Nodes.ComplexNode> implements Representations.Representation {
    constructor(protected node: T, protected representationStorage: Representations.RepresentationStateStorage, protected parentElement: Element) {
    }

    public getNode(): T {
        return this.node;
    }

    public getParentElement(): Element {
        return this.parentElement;
    }

    public getRepresentationStorage(): Representations.RepresentationStateStorage {
        return this.representationStorage
    }
}

export class ComplexTabRepresentation<T extends Nodes.ComplexNode> extends TabRepresentation<T> implements Representations.ComplexRepresentation {
    protected elements: {
        title: JQuery; definition: JQuery; nav: JQuery; open: boolean;
        titleRepresentation: Representations.Representation; definitionRepresentation: Representations.Representation; definingNode: Nodes.Node
    }[] = [];
    protected type = "ComplexTab";

    constructor(node: T, representationStorage: Representations.RepresentationStateStorage, parentElement: Element,
        protected elementReferences: { [id: string]: JQuery }, protected nodeConverterRegistry: NodeConverters.NodeConverterRegistry) {
        super(node, representationStorage, parentElement);
    }

    public getElementReferences(): { [id: string]: JQuery } {
        return this.elementReferences;
    }

    public getNodeConverterRegistry(): NodeConverters.NodeConverterRegistry {
        return this.nodeConverterRegistry;
    }

    protected getValue(id: number): Nodes.Node {
        throw "Abstract function not implemented";
    }

    public open(id: string | number, untilLoop: boolean = false): Representations.Representation {
        if ((typeof id !== "number") || (this.elements[id] === undefined) || (this.elements[id].nav === null) ||
            (untilLoop && (typeof this.elementReferences[this.getValue(id).getId()] !== "undefined"))) {
            return null;
        } else {
            this.elements[id].nav.addClass("open");
            var value = this.getValue(id);
            if (this.elements[id].definitionRepresentation === null) {
                this.elements[id].definition.empty();
                Helpers.hide(this.elements[id].definition, true);
                var newReferences = ObjectHelper.clone(this.elementReferences);
                newReferences[value.getId()] = this.elements[id].title.add(this.elements[id].definition);
                var dummy = $("<div>").css("visibility", "hidden").insertBefore(this.elements[id].definition);
                this.elements[id].definition.detach(); // speed up DOM work
                this.elements[id].definitionRepresentation = this.nodeConverterRegistry.convert(value,
                    this.elements[id].definition[0], newReferences, this.representationStorage);
                this.elements[id].definition.insertAfter(dummy);
                dummy.remove();
                this.elements[id].open = false;
            }
            if (!this.elements[id].open) {
                Helpers.show(this.elements[id].definition);
                this.elements[id].open = true;
            }
            if (untilLoop && (value instanceof Nodes.ComplexNode))
                value.getElements().forEach((element, index, array) => {
                    (<Representations.ComplexNodeRepresentation<Nodes.ComplexNode>>this.elements[id].definitionRepresentation).open(index, true);
                }, this);
            return this.elements[id].definitionRepresentation;
        }
    }

    public close(id: string | number, immidiate: boolean = false): boolean {
        if ((typeof id !== "number") || (typeof this.elements[id] === "undefined") || (this.elements[id].nav === null)) {
            return false;
        } else {
            this.elements[id].nav.removeClass("open");
            Helpers.hide(this.elements[id].definition, immidiate);
            this.elements[id].open = false;
            return true;
        }
    }

    public reset(id: string | number): void {
        if ((typeof id === "number") && (typeof this.elements[id] !== "undefined") && (this.elements[id].nav !== null)) {
            this.elements[id].nav.removeClass("open");
            Helpers.hide(this.elements[id].definition, false, true);
            this.elements[id].definitionRepresentation = null;
            this.elements[id].open = false;
        }
    }

    public switch(id: string | number, complete: boolean = false): void {
        if ((typeof id !== "number") || (typeof this.elements[id] === "undefined") || (this.elements[id].nav === null)) {
            return;
        } else {
            if (!this.elements[id].open)
                this.open(id, complete);
            else if (complete)
                this.reset(id);
            else
                this.close(id);
        }
    }

    public getState(): { type: string; elements: { id: number; open: boolean; nodeType: string; criteria: string; state: any }[] } {
        var elements = this.elements.map((element, id) => {
            if ((element.definitionRepresentation === null) || (element.nav === null))
                return null;
            var node = element.definingNode;
            var state: any = null;
            if ((element.definitionRepresentation !== null) && Helpers.isComplexRepresentation(element.definitionRepresentation))
                state = (<Representations.ComplexRepresentation>element.definitionRepresentation).getState();
            return { id: id, open: element.open, nodeType: node.getNodeType(), criteria: node.getCriteriaString(), state: state };
        });
        return { type: this.type, elements: elements };
    }

    public setState(state: { type: string; elements: { id: number; open: boolean; nodeType: string; criteria: string; state: any }[] }): void {
        if ((state.type !== this.type) || (this.elements.length !== state.elements.length)) {
            this.elements.forEach((element, id) => this.reset(id));
        } else {
            // change state
            this.elements.forEach((element, id) => {
                var node = element.definingNode;
                if ((typeof state.elements[id] !== "undefined") && (state.elements[id] !== null) &&
                    (state.elements[id].nodeType === node.getNodeType()) && (state.elements[id].criteria === node.getCriteriaString())) {
                    this.open(id);
                    if (!state.elements[id].open) this.close(id, true);
                    if ((element.definitionRepresentation !== null) && Helpers.isComplexRepresentation(element.definitionRepresentation)
                        && (state.elements[id].state !== null))
                        (<Representations.ComplexRepresentation>element.definitionRepresentation).setState(state.elements[id].state);
                } else {
                    this.reset(id);
                }
            });
        }
    }

    public isOpen(id: string | number) {
        return (typeof id === "number") && (typeof this.elements[id] !== "undefined") && this.elements[id].open;
    }
}

//#endregion

//#region Array TabRepresentations

export class ArrayElementsTabRepresentation extends ComplexTabRepresentation<Nodes.ArrayNode> {
    constructor(node: Nodes.ArrayNode, representationStorage: Representations.RepresentationStateStorage, parentElement: Element,
        elementReferences: { [id: string]: JQuery }, nodeConverterRegistry: NodeConverters.NodeConverterRegistry) {
        super(node, representationStorage, parentElement, elementReferences, nodeConverterRegistry);
        this.type = "ArrayElements";
        // TODO categories to filter
        // get and render template
        var tmpl = Template.get("ArrayElements");
        var elementReferences = elementReferences;
        var data = {
            elements: node.getElements(),
            isComplex: (element: Nodes.Node) => element instanceof Nodes.ComplexNode,
            elementReferences: elementReferences
        };
        var html = tmpl.render(data);
        // add to parent
        var parent = $(parentElement);
        parent.html(html);
        Helpers.trimHtml(parentElement);
        // add converted elements
        var titles = parent.children("dl").children("dt");
        titles.each((i, el) => Helpers.trimHtml(el));
        var definitions = parent.children("dl").children("dd");
        definitions.empty();
        for (var key in data.elements) {
            var k = +key; // convert to number
            if (typeof k !== "number" || k !== k) continue; // number/NaN check
            var element = data.elements[k];
            var title = titles.eq(k).children("span.term");
            var definition: JQuery;
            var nav: JQuery = null;
            var titleRepresentation: Representations.Representation;
            var definitionRepresentation: Representations.Representation = null;
            var newReferences = ObjectHelper.clone(elementReferences);
            newReferences[element.getValue().getId()] = titles.eq(k).add(definitions.eq(k));

            // add key content
            titleRepresentation = nodeConverterRegistry.convert(
                element.getKey(), title[0], newReferences, this.getRepresentationStorage());
            if (element.getValue() instanceof Nodes.ComplexNode) {
                // add onclick & ondblclick to nav
                nav = titles.eq(k).children("nav").eq(0);
                nav.click({ representation: this, id: k }, Helpers.switchStateClick); // TODO move to global handler?
                nav.dblclick({ representation: this, id: k }, Helpers.resetStateClick);
                if ((<Nodes.ComplexNode>element.getValue()).isEmpty())
                    nav.addClass("empty");
                // add onclick to span.id if duplicate
                if (element.getValue().getId() in elementReferences) {
                    var id = titles.eq(k).children("span.id").eq(0);
                    var er = elementReferences[element.getValue().getId()];
                    id.click(er, Helpers.scrollToOriginalEvent);
                    id.hover((event) => { er.addClass("markedDuplicate"); }, (event) => { er.removeClass("markedDuplicate"); });
                }
                definition = definitions.eq(k);
            } else {
                definition = titles.eq(k).children("span.definition");
                // add value content
                definitionRepresentation = nodeConverterRegistry.convert(
                    element.getValue(), definition[0], newReferences, this.getRepresentationStorage());
            }
            // copy classes to title
            definitions.eq(k).attr('class').split(/\s+/).forEach((className) => titles.eq(k).addClass(className));
            // save element references
            this.elements[k] = {
                title: title, definition: definition, nav: nav, open: !(element.getValue() instanceof Nodes.ComplexNode),
                titleRepresentation: titleRepresentation, definitionRepresentation: definitionRepresentation,
                definingNode: element.getKey()
            };
        }
    }

    protected getValue(id: number): Nodes.Node {
        return this.node.getElements()[id].getValue();
    }
}

//#endregion

//#region Object TabRepresentations

export class ObjectPropertiesTabRepresentation extends ComplexTabRepresentation<Nodes.ObjectNode> {
    protected sortedProperties: Nodes.ObjectProperty[];

    constructor(node: Nodes.ObjectNode, representationStorage: Representations.RepresentationStateStorage, parentElement: Element,
        elementReferences: { [id: string]: JQuery }, nodeConverterRegistry: NodeConverters.NodeConverterRegistry) {
        super(node, representationStorage, parentElement, elementReferences, nodeConverterRegistry);
        this.type = "ObjectProperties";
        // TODO categories to filter
        // get and render template
        var tmpl = Template.get("ObjectProperties");
        var elementReferences = elementReferences;
        // sort properties by visibility
        this.sortedProperties = node.getElements().sort((a, b) =>
            ObjectPropertiesTabRepresentation.getPropertyWeight(a) - ObjectPropertiesTabRepresentation.getPropertyWeight(b));
        var data = {
            properties: this.sortedProperties,
            isComplex: (element: Nodes.Node) => element instanceof Nodes.ComplexNode,
            elementReferences: elementReferences,
            declaringClass: node.getType()
        };
        var html = tmpl.render(data);
        // add to parent
        var parent = $(parentElement);
        parent.html(html);
        Helpers.trimHtml(parentElement);
        // add converted elements
        var titles = parent.children("dl").children("dt");
        titles.each((i, el) => Helpers.trimHtml(el));
        var definitions = parent.children("dl").children("dd");
        definitions.each((i, el) => Helpers.trimHtml(el));
        for (var key in data.properties) {
            var k = +key; // convert to number
            if (typeof k !== "number" || k !== k) continue; // number/NaN check
            var property = data.properties[k];
            var definition: JQuery;
            var nav: JQuery = null;
            var definitionRepresentation: Representations.Representation = null;
            var newReferences = ObjectHelper.clone(elementReferences);
            newReferences[property.getValue().getId()] = titles.eq(k).add(definitions.eq(k));

            if (property.getValue() instanceof Nodes.ComplexNode) {
                // add onclick & ondblclick to nav
                nav = titles.eq(k).children("nav").eq(0);
                nav.click({ representation: this, id: k }, Helpers.switchStateClick);
                nav.dblclick({ representation: this, id: k }, Helpers.resetStateClick);
                if ((<Nodes.ComplexNode>property.getValue()).isEmpty())
                    nav.addClass("empty");
                // add onclick to span.id if duplicate
                if (property.getValue().getId() in elementReferences) {
                    var id = titles.eq(k).children("span.id").eq(0);
                    var er = elementReferences[property.getValue().getId()];
                    id.click(er, Helpers.scrollToOriginalEvent);
                    id.hover((event) => { er.addClass("markedDuplicate"); }, (event) => { er.removeClass("markedDuplicate"); });
                }
                definition = definitions.eq(k);
            } else {
                definition = titles.eq(k).children("span.definition");
                titles.eq(k).children("span.term").each((i, el) => Helpers.trimHtml(el));
                // add value content
                definitionRepresentation = nodeConverterRegistry.convert(
                    property.getValue(), definition[0], newReferences, this.getRepresentationStorage());
            }
            // copy classes to title
            definitions.eq(k).attr('class').split(/\s+/).forEach((className) => titles.eq(k).addClass(className));
            // save element references
            this.elements[k] = {
                title: titles.eq(k), definition: definition, nav: nav, open: !(property.getValue() instanceof Nodes.ComplexNode),
                titleRepresentation: null, definitionRepresentation: definitionRepresentation,
                definingNode: property.getValue()
            };
        }
    }

    protected static getPropertyWeight(property: Nodes.ObjectProperty): number {
        var result = property.getIsStatic() ? 0 : 100;
        if (property.getVisibility() === "private") result = result + 10;
        if (property.getVisibility() === "protected") result = result + 20;
        if (property.getVisibility() === "public") result = result + 30;
        return result;
    }

    protected getValue(id: number): Nodes.Node {
        return this.sortedProperties[id].getValue();
    }
}

//#endregion

//#region CallFrames TabRepresentations

export class CallFrameArgumentsTabRepresentation<T extends Nodes.CallFrameNode> extends ComplexTabRepresentation<T> {
    private arrayElements: ArrayElementsTabRepresentation;

    constructor(node: T, representationStorage: Representations.RepresentationStateStorage, parentElement: Element,
        elementReferences: { [id: string]: JQuery }, nodeConverterRegistry: NodeConverters.NodeConverterRegistry) {
        super(node, representationStorage, parentElement, elementReferences, nodeConverterRegistry);
        this.arrayElements = new ArrayElementsTabRepresentation(node.getArgs(), representationStorage, parentElement,
            elementReferences, nodeConverterRegistry);
    }

    public getArrayElementTabRepresentation(): ArrayElementsTabRepresentation {
        return this.arrayElements;
    }

    protected getValue(id: number): Nodes.CallFrameNode {
        throw "Should be unused";
    }

    public open(id: string | number): Representations.Representation {
        throw "Should be unused";
    }

    public close(id: string | number, immidiate?: boolean): boolean {
        throw "Should be unused";
    }

    public reset(id: string | number): void {
        throw "Should be unused";
    }

    public switch(id: string | number): void {
        throw "Should be unused";
    }

    public getState(): { type: string; arrayElements: any; elements: { id: number; open: boolean; nodeType: string; criteria: string; state: any }[] } {
        return { type: "CallFrameArguments", arrayElements: this.arrayElements.getState(), elements: [] };
    }

    public setState(state: { type: string; arrayElements: any; elements: { id: number; open: boolean; nodeType: string; criteria: string; state: any }[] }): void {
        if ((state.type === "CallFrameArguments")) {
            this.arrayElements.setState(state.arrayElements);
        } else {
            this.arrayElements.setState({ type: null, elements: [] });
        }
    }

    public isOpen(id: string | number): boolean {
        throw "Should be unused";
    }
}

export class SourceTabRepresentation extends TabRepresentation<Nodes.CallFrameNode> {
    constructor(node: Nodes.CallFrameNode, representationStorage: Representations.RepresentationStateStorage, parentElement: Element,
        elementReferences: { [id: string]: JQuery }, nodeConverterRegistry: NodeConverters.NodeConverterRegistry) {
        super(node, representationStorage, parentElement);
        if (node.getSource() === null)
            throw "Source cannot be null";
        var parent = $(parentElement);
        parent.empty();
        parent.addClass("source");
        if (node.getSource() instanceof Nodes.FileSource)
            parent.append($("<span>").addClass("path").text((<Nodes.FileSource>node.getSource()).getPath()));
        var content = parent.append("<div>").addClass("content");
        nodeConverterRegistry.convert(node.getSource().getContentNode(), content[0], elementReferences, representationStorage);
    }
}

export class ClosureHandleTabRepresentation extends TabRepresentation<Nodes.ClosureCallFrameNode> {
    constructor(node: Nodes.ClosureCallFrameNode, representationStorage: Representations.RepresentationStateStorage, parentElement: Element,
        elementReferences: { [id: string]: JQuery }, nodeConverterRegistry: NodeConverters.NodeConverterRegistry) {
        super(node, representationStorage, parentElement);
        if (node.getTargetClosureHandle() === null)
            throw "ClosureHandle cannot be null";
        var parent = $(parentElement);
        parent.empty();
        parent.addClass("closureHandle");
        if (node.getTargetClosureHandle().getSource() instanceof Nodes.FileSource)
            parent.append($("<span>").addClass("path").text((<Nodes.FileSource>node.getTargetClosureHandle().getSource()).getPath()));
        var content = parent.append("<div>").addClass("content");
        nodeConverterRegistry.convert(node.getTargetClosureHandle().getContent(), content[0], elementReferences, representationStorage);
    }
}

//#endregion
