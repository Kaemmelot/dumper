﻿/// <reference path="../../typings/index.d.ts" />
import $ = require("jquery");
import Template = require("./Template");
import Nodes = require("./Nodes");
import Helpers = require("./Helpers");
import Representations = require("./Representations");
import TabRepresentations = require("./TabRepresentations");
import NodeConverters = require("./NodeConverters");

export class SessionStorageRepresentationState implements Representations.RepresentationStateStorage {
    constructor(private rootRepresentation: Representations.RootRepresentation) {
    }

    saveCurrentState(): void {
        var state = this.rootRepresentation.getState();
        sessionStorage.setItem("kaemmelot-php-dump-" + this.rootRepresentation.getId(), JSON.stringify(state));
    }

    restoreSavedState(): void {
        var session = sessionStorage.getItem("kaemmelot-php-dump-" + this.rootRepresentation.getId());
        var state: { type: string };
        if ((typeof session !== "undefined") && ((state = JSON.parse(session)) !== null)) {
            this.rootRepresentation.setState(state);
        } else {
            this.rootRepresentation.setState({ type: null });
        }
    }
}

export class JavascriptHtmlDumper implements Representations.RootRepresentation, Representations.ComplexRepresentation {
    public static get showCallingLineOnlyForComplex(): boolean { return true; } // TODO config
    public static get publishDumperToWindow(): boolean { return true; } // TODO config

    private nodeRepresentation: Representations.NodeRepresentation<Nodes.Node> = null;
    private nodeParent: JQuery;
    private nodeHeaderParent: JQuery;
    private nodeOpen: boolean = false;
    private nodeNav: JQuery = null;
    private traceRepresentation: Representations.ComplexNodeRepresentation<Nodes.StackTraceNode> = null;
    private traceParent: JQuery;
    private traceHeaderParent: JQuery;
    private traceOpen: boolean = false;
    private traceNav: JQuery = null;
    private representationStorage: Representations.RepresentationStateStorage;

    constructor(private node: Nodes.Node, private nodes: { [id: string]: Nodes.Node }, private trace: Nodes.StackTraceNode, private callingLine: string,
        private parentElement: Element, private id: string,
        representationStorageFactory: (rootRepresentation: Representations.RootRepresentation) => Representations.RepresentationStateStorage) {
        // get and render template
        var tmpl = Template.get("JavascriptHtmlDumper");
        var data = { containsTrace: trace !== null, expandable: node.isExpandable() };
        var html = tmpl.render(data);
        // add to parent
        var parent = $(parentElement);
        var dummy = $("<div>").css("visibility", "hidden").insertBefore(parent);
        parent.detach(); // speed up DOM work
        var elements = $(html);
        parent.append(elements);
        Helpers.trimHtml(parentElement);
        // add node and trace to nodes
        if ((nodes[node.getId()] === undefined) || (nodes[node.getId()] === null))
            this.nodes[node.getId()] = node;
        if ((trace !== null) && ((nodes[trace.getId()] === undefined) || (nodes[trace.getId()] === null)))
            this.nodes[trace.getId()] = trace;

        this.nodeHeaderParent = elements.filter("div.javascript-html-header").eq(0);
        Helpers.trimHtml(this.nodeHeaderParent[0]);
        this.representationStorage = representationStorageFactory(this);
        this.nodeParent = elements.filter("div.javascript-html-dump").eq(0);
        Helpers.hide(this.nodeParent, true);
        // print header
        if ((!JavascriptHtmlDumper.showCallingLineOnlyForComplex || (node instanceof Nodes.ComplexNode)) && (callingLine !== null))
            this.nodeHeaderParent.children("span.outline").html(Template.htmlEncode(callingLine, false));
        else if (node instanceof Nodes.ComplexNode)
            this.nodeHeaderParent.children("span.outline").html((trace === null) ?
                "<span class=\"function\">dump</span>(" + node.getOutline() + ");"
                : trace.getMostRecentCallFrame().getOutline());
        else
            this.nodeHeaderParent.children("span.outline").html(node.getOutline());
        // add possible nav
        if (data.expandable) {
            if (!(node instanceof Nodes.ComplexNode))
                this.nodeParent.addClass("simpleNode");
            this.nodeNav = this.nodeHeaderParent.children("nav").eq(0);
            this.nodeNav.click({ representation: this, id: "node" }, Helpers.switchStateClick)
                .dblclick({ representation: this, id: "node" }, Helpers.resetStateClick);
        } else
            this.nodeHeaderParent.addClass("simpleNode");

        // process trace part
        this.traceHeaderParent = elements.filter("div.javascript-html-called-from-header").eq(0);
        this.traceParent = elements.filter("div.javascript-html-called-from").eq(0);
        Helpers.hide(this.traceParent, true);
        if (trace !== null) {
            Helpers.trimHtml(this.traceHeaderParent[0]);
            this.traceNav = this.traceHeaderParent.children("nav").eq(0);
            this.traceNav.click({ representation: this, id: "trace" }, Helpers.switchStateClick)
                .dblclick({ representation: this, id: "trace" }, Helpers.resetStateClick);
            // print header
            var source = trace.getMostRecentCallFrame().getSource();
            var sourceLine = trace.getMostRecentCallFrame().getSourceLine();
            var sourcelines = this.traceHeaderParent.children("span.outline").html(Helpers.getOutlineForSource(source, sourceLine));
        }
        $(document).on("dblclick", "span.type, span.sourceline, span.filepath:not(span.sourceline > span.filepath), span.string:not(:has(.tripleDot))", Helpers.selectElement);
        if (JavascriptHtmlDumper.publishDumperToWindow) { // TODO config
            window.kaemmelotDumper = window.kaemmelotDumper || {};
            window.kaemmelotDumper[this.id] = this;
        }
        parent.insertAfter(dummy);
        dummy.remove();
        // open/close last state
        this.representationStorage.restoreSavedState();
    }

    public getParentElement(): Element {
        return this.parentElement;
    }

    public getRepresentationStorage(): Representations.RepresentationStateStorage {
        return this.representationStorage;
    }

    public getNodes(): { [id: string]: Nodes.Node } {
        return this.nodes;
    }

    public getNodeById(id: string): Nodes.Node {
        return this.nodes[id];
    }

    public getId(): string {
        return this.id;
    }

    public open(id: string | number, untilLoop: boolean = false): Representations.NodeRepresentation<any> {
        if ((id === "node") && this.node.isExpandable()) {
            if (this.nodeRepresentation === null) {
                Helpers.hide(this.nodeParent, true);
                var elementReferences: { [id: string]: JQuery } = {};
                elementReferences[this.node.getId()] = this.nodeHeaderParent.add(this.nodeParent);
                var dummy = $("<div>").css("visibility", "hidden").insertBefore(this.nodeParent);
                this.nodeParent.detach(); // speed up DOM work
                this.nodeRepresentation = NodeConverters.convertToRepresentation(this.node, this.nodes, this.nodeHeaderParent.add(this.nodeParent),
                    this.nodeParent[0], this.representationStorage, elementReferences);
                this.nodeParent.insertAfter(dummy);
                dummy.remove();
            }
            Helpers.show(this.nodeParent);
            this.nodeOpen = true;
            this.nodeNav.addClass("open");
            if (untilLoop && (this.node instanceof Nodes.ComplexNode))
                (<Nodes.ComplexNode>this.node).getElements().forEach((element, index, array) => {
                    (<Representations.ComplexNodeRepresentation<Nodes.ComplexNode>>this.nodeRepresentation).open(index, true);
                }, this);
            return this.nodeRepresentation;
        } else if ((id === "trace") && (this.trace !== null)) {
            if (this.traceRepresentation === null) {
                Helpers.hide(this.traceParent, true, false, () => { this.traceHeaderParent.removeClass("show"); });
                var elementReferences: { [id: string]: JQuery } = {};
                elementReferences[this.trace.getId()] = this.traceHeaderParent.add(this.traceParent);
                var dummy = $("<div>").css("visibility", "hidden").insertBefore(this.traceParent);
                this.traceParent.detach(); // speed up DOM work
                this.traceRepresentation = <Representations.ComplexNodeRepresentation<Nodes.StackTraceNode>>
                    NodeConverters.convertToRepresentation(this.trace, this.nodes, this.nodeHeaderParent.add(this.nodeParent),
                        this.traceParent[0], this.representationStorage, elementReferences);
                this.traceParent.insertAfter(dummy);
                dummy.remove();
            }
            Helpers.show(this.traceParent);
            this.traceHeaderParent.addClass("show");
            this.traceOpen = true;
            this.traceNav.addClass("open");
            if (untilLoop)
                this.trace.getElements().forEach((element, index, array) => { this.traceRepresentation.open(index, true); }, this);
            return this.traceRepresentation;
        } else
            return null;
    }

    public close(id: string | number, immidiate: boolean = false): boolean {
        if ((id === "node") && this.node.isExpandable()) {
            Helpers.hide(this.nodeParent);
            this.nodeOpen = false;
            this.nodeNav.removeClass("open");
        } else if ((id === "trace") && (this.trace !== null)) {
            Helpers.hide(this.traceParent, false, false, () => { this.traceHeaderParent.removeClass("show"); });
            this.traceOpen = false;
            this.traceNav.removeClass("open");
        } else
            return false;
        return true;
    }

    public switch(id: string | number, complete: boolean = false): void {
        var open: boolean;
        if (id === "node")
            open = this.nodeOpen;
        else if (id === "trace")
            open = this.traceOpen;
        else
            return;

        if (open && !complete)
            this.close(id);
        else if (open)
            this.reset(id);
        else
            this.open(id, complete);
    }

    public reset(id: string | number): void {
        if ((id === "node") && this.node.isExpandable()) {
            Helpers.hide(this.nodeParent, false, true);
            this.nodeOpen = false;
            this.nodeRepresentation = null;
            this.nodeNav.removeClass("open");
        } else if ((id === "trace") && (this.trace !== null)) {
            Helpers.hide(this.traceParent, false, true, () => { this.traceHeaderParent.removeClass("show"); });
            this.traceOpen = false;
            this.traceRepresentation = null;
            this.traceNav.removeClass("open");
        }
    }

    public getState(): { type: String; node: any; nodeComplex: boolean; nodeOpen: boolean; trace: any; traceOpen: boolean } {
        var node: any = null,
            nodeComplex: boolean,
            trace: any = null;
        if ((this.nodeRepresentation !== null) && (nodeComplex = Helpers.isComplexRepresentation(this.nodeRepresentation)))
            node = (<Representations.ComplexRepresentation>(<any>this.nodeRepresentation)).getState();
        else if (this.nodeRepresentation !== null)
            node = this.node.getCriteriaString();
        if (this.traceRepresentation !== null)
            trace = this.traceRepresentation.getState();
        return { type: "JavascriptHtmlDumper", node: node, nodeComplex: nodeComplex, nodeOpen: this.nodeOpen, trace: trace, traceOpen: this.traceOpen };
    }

    public setState(state: { type: String; node: any; nodeComplex: boolean; nodeOpen: boolean; trace: any; traceOpen: boolean }): void {
        if (state.type === "JavascriptHtmlDumper") {
            if (state.node !== null) {
                this.open("node");
                if (!state.nodeOpen)
                    this.close("node", true);
                if (Helpers.isComplexRepresentation(this.nodeRepresentation) && state.nodeComplex) // fits
                    (<Representations.ComplexRepresentation>(<any>this.nodeRepresentation)).setState(state.node);
                else if (Helpers.isComplexRepresentation(this.nodeRepresentation)) // does not fit
                    (<Representations.ComplexRepresentation>(<any>this.nodeRepresentation)).setState({ type: null });
                else if (this.node.getCriteriaString() !== state.node) // does not fit
                    this.close("node", true);
            } else
                this.reset("node");
            if ((this.trace !== null) && (state.trace !== null)) {
                this.open("trace");
                if (!state.traceOpen)
                    this.close("trace", true);
                this.traceRepresentation.setState(state.trace);
            } else
                this.reset("trace");
        } else {
            this.close("node", true);
            this.reset("node");
            this.close("trace", true);
            this.reset("trace");
        }
    }

    public isOpen(id: string | number): boolean {
        return ((id === "node") && this.nodeOpen) || ((id === "trace") && this.traceOpen);
    }
}
