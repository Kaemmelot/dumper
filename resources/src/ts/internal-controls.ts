/// <reference path="../../typings/index.d.ts" />
import domReady = require("domReady");
import Draggable = require("./Draggable");

domReady(() => {
    var loaded = false;
    var control = $(window.frameElement); // this->outer
    window.addEventListener("message", (event: MessageEvent) => {
        if (event.origin !== window.location.origin)
            return;

        var container = control.parent("div.kaemmelot-dump").eq(0);
        var dumper = container.children("iframe.kaemmelot-dump").eq(0);
        var id = dumper.data("id");
        if ((event.data === "dom-ready") && !loaded) {
            loaded = true;
            var body = $("body");
            var dragMe = $("<div>");
            var draggable = new Draggable.Draggable(false, true, dragMe[0], container[0], container.parents("body")[0], true);
            var minimized = true;
            var minMax = $("<div>", { class: "minMax minimized" });
            body.append(minMax);
            body.append(dragMe);
            dragMe.on("click", (e: JQueryMouseEventObject) => {
                if ((e.button !== 0) || draggable.ignoreOnClick()) // only left button and only if mouse didn't move
                    return;
                if (minimized) {
                    minMax.removeClass("minimized").addClass("maximized");
                    control.attr("style", (i, style) => {
                        return style.replace(/height:\s*[0-9]+px/, "height: " + body[0].scrollHeight + "px"); // JQuerys (outer)height() doesn't work here!
                    });
                    var height = Math.max(body[0].scrollHeight, parseInt(dumper.css("height"), 10) || 0); // Find largest element
                    container.attr("style", (i, style) => {
                        return style.replace(/height:\s*[0-9]+px/, "height: " + height + "px");
                    });
                    dumper.attr("style", (i, style) => {
                        return style.replace(/visibility:\s*hidden/, "visibility: visible");
                    });
                } else { // TODO container width on outer scrollbar (no overlapping)
                    dumper.attr("style", (i, style) => {
                        return style.replace(/visibility:\s*visible/, "visibility: hidden");
                    });
                    control.attr("style", (i, style) => {
                        return style.replace(/height:\s*[0-9]+px/, "height: 26px");
                    });
                    container.attr("style", (i, style) => {
                        return style.replace(/height:\s*[0-9]+px/, "height: 26px");
                    });
                    minMax.removeClass("maximized").addClass("minimized");
                }
                minimized = !minimized;
            });
            var popup = $("<div>", { class: "popup" });
            body.append(popup);
            popup.on("click", (e: JQueryMouseEventObject) => {
                window.parent.postMessage({ type: "popup", id: id }, window.location.origin);
            });
        }
    });
    window.loaded = true;
});
