﻿/// <reference path="../../typings/index.d.ts" />
/*
 * This template engine is based on Teleriks inside the Kendo-UI-Core
 * which has been released under the Apache License, version 2.0, that
 * can be obtained at http://www.apache.org/licenses/LICENSE-2.0. The
 * Kendo-UI-Core can be found at https://github.com/telerik/kendo-ui-core.
 * 
 * This file partly uses
 * https://github.com/telerik/kendo-ui-core/blob/277810bc9a2a23b61e04fd25ef2f4e800be15a88/src/kendo.core.js
 * but has been modified and converted to typescript.
 * 
 * The idea of this engine is originally based on the work of John Resig from
 * http://ejohn.org/blog/javascript-micro-templating/ and improvements
 * of Rick Strahl from
 * http://weblog.west-wind.com/posts/2008/Oct/13/Client-Templating-with-jQuery
 *
 * It's possible to create a template by using the Template-constructor or
 * by Template.loadFromTemplateFile(). Templates get compiled and cached so
 * they can be accessed by their name with Template.get();
 * A template file has to contain <script>-Tags with type="text/template" and
 * an id which is used as name.
 * Use #<commands># in templates to surround javascript (<commands>) that should
 * be executed. #=<value># will print the <value> at the current position (html encoded).
 * #~<string># is a special case, that will replace spaces, tabs and newlines
 * with their html equivalent (formatted string).
 * 
 * Notice: Uses JQuery.
 */

import $ = require("jquery");

export function htmlEncode(value: string, format: boolean): string {
    var result = ("" + value)
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#39;");
    if (format)
        result = result.replace(/\t/, "&nbsp;&nbsp;&nbsp;&nbsp;").replace(/[^\S\n]/g, "&nbsp;").replace(/\n/g, "<br />");
    return result;
}

export function get(templateName: string): Template {
    if (Template.getLoadedTemplates().hasOwnProperty(templateName))
        return Template.getLoadedTemplates()[templateName];
    else
        return null;
}

export function loadTextFromTemplateFile(url: string, callback: (text: { [name: string]: string }) => void): void {
    var text: { [name: string]: string } = {};
    $.ajax({
        url: url,
        context: this,
        success: function (result: string) {
            $(result).each(function (idx, element) {
                var e = $(element);
                if (e.is("script") && (e.attr("type") == "text/template") && (e.attr("id").length > 0))
                    text[e.attr("id")] = e.html();
            });
            callback(text);
        },
        error: function (jqXHR, textStatus, errorThrown) { throw new Error("Could not load templates from " + url + ": " + errorThrown); }
    });
}

export function getFromText(text: { [name: string]: string }): { [name: string]: Template } {
    var templates: { [name: string]: Template } = {};
    for (var id in text) {
        if (text.hasOwnProperty(id))
            templates[id] = new Template(id, text[id]);
    }
    return templates;
}

export function loadFromTemplateFile(url: string, callback: (templates: { [name: string]: Template }) => void = null): void {
    loadTextFromTemplateFile(url, (text) => {
        var templates = getFromText(text);
        if (callback)
            callback(templates);
    });
}

export class Template {
    private static argumentNameRegExp = /^\w+/;
    private static encodeRegExp = /\$\{([^}]*)\}/g;
    private static escapedCurlyRegExp = /\\\}/g;
    private static curlyRegExp = /__CURLY__/g;
    private static escapedSharpRegExp = /\\#/g;
    private static sharpRegExp = /__SHARP__/g;
    private static loadedTemplates: { [name: string]: Template } = {};

    private compiledTemplate: (data: any, $htmlEncode: (html: string, format: boolean) => string) => string;

    constructor(private templateName: string, private template: string, private paramName: string = "data", private useWithBlock: boolean = true) {
        if (Template.loadedTemplates.hasOwnProperty(templateName))
            throw new Error("Template already exists! Use Template.get() instead.");

        var argumentName = paramName.match(Template.argumentNameRegExp)[0],
            functionBody = "var $templateOutput;",
            parts: string[];

        functionBody += useWithBlock ? "with(" + paramName + "){" : "";
        functionBody += "$templateOutput=";

        parts = template
            .replace(Template.escapedCurlyRegExp, "__CURLY__")
            .replace(Template.encodeRegExp, "#=$htmlEncode($1,false)#")
            .replace(Template.curlyRegExp, "}")
            .replace(Template.escapedSharpRegExp, "__SHARP__")
            .split("#");

        for (var idx = 0; idx < parts.length; idx++) {
            functionBody += this.compilePart(parts[idx], idx % 2 === 0);
        }

        functionBody += useWithBlock ? ";}" : ";";
        functionBody += "return $templateOutput;";
        functionBody = functionBody.replace(Template.sharpRegExp, "#");

        try {
            this.compiledTemplate = <any>new Function(argumentName, "$htmlEncode", functionBody);
        } catch (e) {
            throw new Error("Invalid template " + templateName + ":\n'" + template + "'\nError:\n'" + e + "'\nGenerated code:\n" + functionBody);
        }
        Template.loadedTemplates[templateName] = this;
    }

    public static getLoadedTemplates(): { [name: string]: Template } {
        return this.loadedTemplates;
    }

    private compilePart(part: string, stringPart: boolean): string {
        if (stringPart) {
            return "'" + part
                .split("'").join("\\'")
                .split('\\"').join('\\\\\\"')
                .replace(/\n/g, "\\n")
                .replace(/\r/g, "\\r")
                .replace(/\t/g, "\\t") + "'";
        } else {
            var first = part.charAt(0),
                rest = part.substring(1);

            if (first === "=") {
                return "+(" + rest + ")+";
            } else if (first === ":") {
                return "+$htmlEncode(" + rest + ", false)+";
            } else if (first === "~") {
                return "+$htmlEncode(" + rest + ", true)+";
            } else {
                return ";" + part + ";$templateOutput+=";
            }
        }
    }

    public render(data: any): string {
        return this.compiledTemplate(data, htmlEncode);
    }
}
