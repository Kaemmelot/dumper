﻿
interface Window {
    kaemmelotTemplateTexts: { [id: string]: string };
    kaemmelotDumper: { [index: string]: any };
    kaemmelotRequire: any;
    ResizeSensor: ResizeSensorStatic;
    dumperId: string;
    containerWindow: Window;
    iframe: boolean;
    loaded: boolean;
}
