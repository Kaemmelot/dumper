/// <reference path="../../typings/index.d.ts" />
import _requirejs = require("requirejs"); // Import for type only!
import domReady = require("domReady");
import $ = require("jquery");
import JavascriptHtmlDumperContainer = require("./JavascriptHtmlDumperContainer");
import WaitModal = require("./WaitModal");

declare var require: typeof _requirejs; // already loaded

var imagePath = require.toUrl("").replace("dist/js", "images");
var waitModal = new WaitModal.WaitModal(imagePath + "wait.gif"); // instantly created
domReady(() => {
    var container = new JavascriptHtmlDumperContainer.JavascriptHtmlDumperContainer(waitModal);
    container.init();
});
