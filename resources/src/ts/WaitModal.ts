﻿/// <reference path="../../typings/index.d.ts" />
import $ = require("jquery");

export class WaitModal {
    private waiting = 0;
    private modal: JQuery;
    private callbacks: { callback: { (): void }, allowedCalls: number }[] = [];

    constructor(private pictureUrl: string, private allowIgnore = true) {
        this.init();
    }

    private init(): void {
        // Create wait modal
        this.modal = $("<div>");
        this.modal.addClass("kaemmelot-dump-wait-modal");
        this.modal.attr("style",  // Put on top of screen (a bit transparent) with wait image
            "background: rgba( 255, 255, 255, .8 ) center center fixed no-repeat url(" + this.pictureUrl + ") !important;"
            + " position: fixed !important; z-index: 2147483647 !important; top: 0 !important; left: 0 !important;" // max z-index
            + " height: 100% !important; width: 100% !important; display: block !important;"
        ); // Using !important to ignore all breaking css
        this.modal.appendTo("body");
        if (this.allowIgnore) { // Allow user to ignore waiting
            this.modal.click(() => {
                this.remove();
            });
        }
    }

    public await(count = 1): void {
        this.waiting += count;
        if (this.modal === null)
            this.init();
    }

    public notify(): void {
        this.waiting--;
        if (this.waiting <= 0) {
            for (var id in this.callbacks) { // Execute callbacks when everything is ready
                if (this.callbacks[id].allowedCalls !== 0) {
                    this.callbacks[id].allowedCalls--;
                    this.callbacks[id].callback();
            }
            }
            this.remove();
        }
    }

    public remove(): void {
        if (this.modal !== null) {
            this.modal.remove();
            this.modal = null;
        }
    }

    public addCallback(callback: () => void, allowedCalls = 1) {
        this.callbacks.push({ callback: callback, allowedCalls: allowedCalls });
    }
}
