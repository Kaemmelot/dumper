/// <reference path="../../typings/index.d.ts" />
import _requirejs = require("requirejs"); // Import for type only!
import $ = require("jquery");
import Template = require("./Template");
import WaitModal = require("./WaitModal");

declare var require: typeof _requirejs; // already loaded

export class JavascriptHtmlDumperContainer {
    private dumpers: { [id: string]: { window: Window, json: any, templateReady: boolean } } = {};
    private templatesUnloaded = 0;
    private origin: string;

    constructor(private waitModal: WaitModal.WaitModal) {
        this.origin = window.location.origin;
        this.waitModal.addCallback(this.finish);
    }

    private isContentWindowReady(contentWindow: Window): boolean {
        return (contentWindow !== null && (contentWindow.document.readyState === "complete"))
    }

    private addListener(): void {
        window.addEventListener("message", (event: MessageEvent) => {
            if ((event.origin !== this.origin) || (typeof event.data.type === "undefined"))
                return;

            if (event.data.type === "send-data") {
                var dumper = $(this.dumpers[event.data.id].window.document);
                dumper.children("html").children("body").append("<div class=\"data\" data-id=\"" + event.data.id + "\">" + this.dumpers[event.data.id].json + "</div>");
                this.dumpers[event.data.id].window.postMessage("data-ready", this.origin);
                if (this.templatesUnloaded === 0) {
                    this.dumpers[event.data.id].window.postMessage("templates-ready", this.origin);
                }
                return false;
            }
            if (event.data.type === "dumper-ready") {
                this.waitModal.notify();
                return false;
            }
            if (event.data.type === "popup") {
                var id = event.data.id;
                var container = $("#kaemmelot-php-dump-" + id);
                var url = container.children("iframe.kaemmelot-dump").eq(0).attr("src");
                var options = "height=" + window.outerHeight + ",width=" + window.outerWidth + ",left=0,top=0,location=no" + // TODO not complete left?
                    ",menubar=no,resizable=yes,scrollbars=yes,toolbar=no";
                container.remove();
                var popup = window.open(url, "kaemmelot-dump-" + id, options, false);
                popup.dumperId = id;
                popup.containerWindow = window;
                popup.iframe = false;
                this.dumpers[id].window = popup;
                this.dumpers[id].templateReady = false;
                popup.focus();
                this.waitModal.await();
                var dumperReady = () => {
                    var tries = 0;
                    var i = id;
                    var int = setInterval(() => {
                        if (++tries > 100) clearInterval(int), console.error("Gave up sending dom-ready to dumper", i);
                        if (!popup.loaded) return;
                        clearInterval(int);
                        popup.postMessage("dom-ready", this.origin);
                        if ((this.templatesUnloaded === 0) && !this.dumpers[i].templateReady) {
                            popup.postMessage("templates-ready", this.origin);
                            this.dumpers[i].templateReady = true;
                        }
                    }, 100);
                };
                if (!this.isContentWindowReady(popup))
                    dumper.on("load", dumperReady);
                else
                    dumperReady();
                return false;
            }
        });
    }

    private preloadTemplates(): void {
        window.kaemmelotTemplateTexts = {};
        var callback = (text: { [id: string]: string }) => {
            var id: string;
            for (id in text) {
                if (text.hasOwnProperty(id))
                    window.kaemmelotTemplateTexts[id] = text[id];
            }
            this.templatesUnloaded--;
            if (this.templatesUnloaded === 0) {
                for (id in this.dumpers) {
                    if (this.dumpers.hasOwnProperty(id) && this.dumpers[id].window !== null && this.dumpers[id].window.loaded) {
                        this.dumpers[id].window.postMessage("templates-ready", this.origin);
                        this.dumpers[id].templateReady = true;
                    }
                }
            }
        };
        this.templatesUnloaded += 2;
        Template.loadTextFromTemplateFile(require.toUrl("") + "../templates/Nodes.tmpl.html", callback);
        Template.loadTextFromTemplateFile(require.toUrl("") + "../templates/Tabs.tmpl.html", callback);
    }

    public init(): void { // TODO clean up
        this.addListener();
        this.preloadTemplates();
        var containers = $("div.kaemmelot-dump");
        this.waitModal.await(containers.length); // Wait for these dumpers to finish loading
        window.kaemmelotDumper = {};
        for (var i = 0; i < containers.length; i++) { // Replace css
            var container = containers.eq(i);
            var dumper = container.children("iframe.kaemmelot-dump").eq(0);
            var control = container.children("iframe.kaemmelot-dump-control").eq(0);
            var id: string = dumper.data("id");
            this.dumpers[id] = { window: null, json: dumper.attr("data-json"), templateReady: false };
            var dumperReady = () => {
                this.dumpers[id].window = (<HTMLIFrameElement>dumper[0]).contentWindow;
                this.dumpers[id].window.dumperId = id;
                this.dumpers[id].window.containerWindow = window;
                this.dumpers[id].window.iframe = true;
                var tries = 0;
                var i = id;
                var int = setInterval(() => {
                    if (++tries > 100) clearInterval(int), console.error("Gave up sending dom-ready to dumper", i);
                    if (!this.dumpers[i].window.loaded) return;
                    clearInterval(int);
                    this.dumpers[i].window.postMessage("dom-ready", this.origin);
                    if ((this.templatesUnloaded === 0) && !this.dumpers[i].templateReady) {
                        this.dumpers[i].window.postMessage("templates-ready", this.origin);
                        this.dumpers[i].templateReady = true;
                    }
                }, 100);
            };
            if (dumper.length === 0 || !this.isContentWindowReady((<HTMLIFrameElement>dumper[0]).contentWindow))
                dumper.on("load", dumperReady);
            else
                dumperReady();

            var controlReady = () => {
                var tries = 0;
                var i = id;
                var c = (<HTMLIFrameElement>control[0]);
                var int = setInterval(() => {
                    if (++tries > 100) clearInterval(int), console.error("Gave up sending dom-ready to controls", i);
                    if (!c.contentWindow.loaded) return;
                    clearInterval(int);
                    c.contentWindow.postMessage("dom-ready", this.origin);
                }, 100);
            };
            if (control.length === 0 || !this.isContentWindowReady((<HTMLIFrameElement>control[0]).contentWindow))
                control.on("load", controlReady);
            else
                controlReady();
            window.kaemmelotDumper[id] = { container: container[0], dumper: dumper[0], control: control[0] };
            control.attr("style",
                "margin: 0 !important; padding: 0 !important; position: absolute !important; left: 2px !important; top: 0 !important; " +
                "overflow: hidden !important; height: 26px !important; width: 24px !important; border: 0 !important;");
            dumper.attr("style",
                "position: absolute !important; left: 28px !important; top: 0 !important; border: 0 !important; " +
                "right: 0 !important; overflow-x: scroll !important; overflow-y: visible !important;  width: 100% !important; height: 44px !important;" + // Default height
                "margin: 0 !important; padding: 0 28px 0 0 !important; visibility: hidden !important; transition: display 0.5s ease !important;");
            container.attr("style", container.attr("style") + // Keep hidden
                "display: inline-block !important; position: absolute !important; top: " + (5 + i * 32) + "px !important; height: 26px !important; left: 0 !important; right: 0 !important; " +
                "z-index: 2147483646 !important; -webkit-user-select: none !important; -moz-user-select: none !important; -ms-user-select: none !important; " + // z-index: one layer left
                "user-select: none !important; border: 0 !important; min-width: 300px !important; overflow: hidden !important;");
            // TODO sticky top + respect other Draggables + minimize width
        }
    }

    private finish = () => {
        $("div.kaemmelot-dump").attr("style", (i, style) => { return style.replace(/display:\s*none/, "visibility: visible"); }); // Show
    };
}
