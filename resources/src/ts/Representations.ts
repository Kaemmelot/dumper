﻿import Nodes = require("./Nodes");


export interface RepresentationStateStorage {
    saveCurrentState(): void;
    restoreSavedState(): void;
}

export interface Representation {
    getParentElement(): Element;
    getRepresentationStorage(): RepresentationStateStorage;
}

export interface ComplexRepresentation extends Representation {
    open(id: string | number, untilLoop?: boolean): Representation;
    close(id: string | number, immidiate?: boolean): boolean;
    switch(id: string | number, complete?: boolean): void;
    reset(id: string | number): void;
    getState(): { type: String };
    setState(state: { type: String }): void;
    isOpen(id: string | number): boolean;
}

export interface RootRepresentation extends Representation {
    getNodes(): { [id: string]: Nodes.Node };
    getNodeById(id: string): Nodes.Node;
    getId(): string;
    getState(): { type: String };
    setState(state: { type: String }): void;
}

export class NodeRepresentation<T extends Nodes.Node> implements Representation {
    constructor(protected node: T, protected parentElement: Element, protected representationStorage: RepresentationStateStorage = null) {
    }

    public getNode() {
        return this.node;
    }

    public getParentElement() {
        return this.parentElement;
    }

    public getRepresentationStorage() {
        return this.representationStorage;
    }
}

// FIX for isComplexRepresentaion function
export class ComplexNodeRepresentation<T extends Nodes.ComplexNode> extends NodeRepresentation<T> implements ComplexRepresentation {
    public open(id: string | number, untilLoop?: boolean): Representation {
        throw "Abstract function not implemented";
    }

    public close(id: string | number, immidiate?: boolean): boolean {
        throw "Abstract function not implemented";
    }

    public switch(id: string | number, complete?: boolean): void {
        throw "Abstract function not implemented";
    }

    public reset(id: string | number): void {
        throw "Abstract function not implemented";
    }

    public getState(): { type: String } {
        throw "Abstract function not implemented";
    }

    public setState(state: { type: String }): void {
        throw "Abstract function not implemented";
    }

    public isOpen(id: string | number): boolean {
        throw "Abstract function not implemented";
    }
}
