﻿import Helpers = require("./Helpers");

//#region Abstract Nodes

export class Node {
    constructor(protected id: string) {
    }

    public getId(): string {
        return this.id;
    }

    public getType(): string {
        return null;
    }

    public getNodeType(): string {
        throw "Abstract function not implemented";
    }

    public getCriteriaString(): string {
        throw "Abstract function not implemented";
    }

    public getOutline(): string {
        return null;
    }

    public isExpandable(): boolean {
        return false;
    }
}

export class ComplexNode extends Node {
    constructor(id: string, protected nodeReferences: { [id: string]: Node }) {
        super(id);
    }

    public getElements(): any[] {
        throw "Abstract function not implemented";
    }

    public isEmpty(): boolean {
        throw "Abstract function not implemented";
    }

    public getNodeReferences(): { [id: string]: Node } {
        return this.nodeReferences;
    }

    public isExpandable(): boolean {
        return true;
    }
}

export class CallFrameNode extends ComplexNode {
    constructor(protected args: ArrayNode, protected sourceLine: number, protected source: Source,
        id: string, nodeReferences: { [id: string]: Node }) {
        super(id, nodeReferences);
    }

    public getArgs(): ArrayNode {
        return this.args;
    }

    public getSourceLine(): number {
        return this.sourceLine;
    }

    public getSource(): Source {
        return this.source;
    }

    public getElements(): ArrayElement[] {
        return this.args.getElements();
    }

    public isEmpty(): boolean {
        return this.args.isEmpty();
    }

    public getCriteriaString(): string {
        var result = "";
        var args = this.args.getElements();
        for (var i = 0; i < args.length; i++)
            result += args[i].getKey().getCriteriaString() + ",";
        return result + ((this.source !== null) ? this.source.getSourceType() : null) + "," + this.sourceLine;
    }

    protected getArgumentsString(): string {
        var args = this.args.getElements();
        var parts: string[] = [];
        for (var i = 0; i < Math.min(3, args.length); i++)
            parts.push(String((<PrimitiveNode>args[i].getKey()).getOutline(0, false))
                + ((args[i].getValue().getOutline() !== null) ? " = " + args[i].getValue().getOutline() : ""));
        return "(" + parts.join(", ") + ((args.length > 3) ? ", ..." : "") + ")";
    }

    public getOutline(): string {
        throw "CallFrameNodes need an outline";
    }
}

export class MethodCallFrameNode extends CallFrameNode {
    constructor(protected targetMethodName: string, protected targetClassName: string, args: ArrayNode, sourceLine: number,
        source: Source, id: string, nodeReferences: { [id: string]: Node }) {
        super(args, sourceLine, source, id, nodeReferences);
    }

    public getTargetMethodName(): string {
        return this.targetMethodName;
    }

    public getTargetClassName(): string {
        return this.targetClassName;
    }

    public getType(): string {
        return "MethodCallFrame";
    }

    public getNodeType(): string {
        return "MethodCallFrameNode";
    }

    public getCriteriaString(): string {
        return super.getCriteriaString() + this.targetClassName + this.targetMethodName;
    }
}

//#endregion

//#region Simple Nodes

export class AbortDepthNode extends Node {
    constructor(protected depth: number, id: string) {
        super(id);
    }

    public getDepth(): number {
        return this.depth;
    }

    public getNodeType(): string {
        return "AbortDepthNode";
    }

    public getCriteriaString(): string {
        return this.depth.toString();
    }

    public getOutline(): string {
        return "**MAX DEPTH**";
    }
}

export class UnknownNode extends Node {
    constructor(protected type: string, id: string) {
        super(id);
    }

    public getType(): string {
        return this.type;
    }

    public getNodeType(): string {
        return "UnknownNode";
    }

    public getCriteriaString(): string {
        return this.type;
    }

    public getOutline(): string {
        return "**UNKNOWN**";
    }
}

export class PrimitiveNode extends Node {
    constructor(protected type: string, protected value: any, id: string) {
        super(id);
    }

    public getType(): string {
        return this.type;
    }

    public getValue(): any {
        return this.value;
    }

    public getNodeType(): string {
        return "PrimitiveNode";
    }

    public getCriteriaString(): string {
        return this.type + ":" + String(this.value);
    }

    public getOutline(stringTrimOffset: number = 15, addStringQuotes: boolean = true): string {
        var result = String(this.value);
        if (this.type === "string") {
            result = result.replace(/\t/g, "    ");
            var trimmed: boolean;
            if ((trimmed = (stringTrimOffset > 0) && (result.length > stringTrimOffset + 3)))
                result = result.substr(0, stringTrimOffset);
            result = result
                .replace(/</g, "&lt;")
                .replace(/>/g, "&gt;")
                .replace(/"/g, "\&quot;")
                .replace(/[^\S\n]/g, "&nbsp;")
                .replace(/\n/g, "\\n");
            if (trimmed)
                result += "<span class=\"tripleDot\">...</span>";
            if (addStringQuotes)
                result = "\"" + result + "\"";
        }
        return "<span class=\"" + this.type + (!addStringQuotes ? " unquoted" : "") + "\">" + result + "</span>";
    }

    public isExpandable(): boolean {
        return this.type === "string";
    }
}

//#endregion

//#region Complex Nodes

export class ReferenceNode extends ComplexNode {
    constructor(protected referencedId: string, id: string, nodeReferences: { [id: string]: Node }) {
        super(id, nodeReferences);
        if (!(referencedId in nodeReferences))
            nodeReferences[referencedId] = null;
    }

    public getId(): string {
        var referencedNode = this.getReferencedNode();
        return ((referencedNode !== null) && (referencedNode !== undefined)) ? referencedNode.getId() : this.referencedId;
    }

    public getElements(): any[] {
        var referencedNode = this.getReferencedNode();
        return (referencedNode !== null) ? referencedNode.getElements() : [];
    }

    public isEmpty(): boolean {
        var referencedNode = this.getReferencedNode();
        return (referencedNode !== null) ? referencedNode.isEmpty() : true;
    }

    public getReferencedId(): string {
        return this.referencedId;
    }

    public getReferencedNode(): ComplexNode {
        return <ComplexNode>this.getNodeReferences()[this.referencedId];
    }

    public getType(): string {
        var referencedNode = this.getReferencedNode();
        return (referencedNode !== null) ? referencedNode.getType() : null;
    }

    public getNodeType(): string {
        return "ReferenceNode";
    }

    public getCriteriaString(): string {
        return (this.getReferencedNode() !== null) ? this.getReferencedNode().getCriteriaString() : null;
    }

    public getOutline(): string {
        return (this.getReferencedNode() !== null) ? this.getReferencedNode().getOutline() : null;
    }

    public isExpandable(): boolean {
        return (this.getReferencedNode() !== null) ? this.getReferencedNode().isExpandable() : true;
    }
}

export class ArrayElement {
    constructor(protected key: Node, protected value: Node) {
    }

    public getKey(): Node {
        return this.key;
    }

    public getValue(): Node {
        return this.value;
    }
}

export class ArrayNode extends ComplexNode {
    constructor(protected items: ArrayElement[], id: string, nodeReferences: { [id: string]: Node }, protected outline: string = null) {
        super(id, nodeReferences);
    }

    public getElements(): ArrayElement[] {
        return this.items;
    }

    public isEmpty(): boolean {
        return this.items.length === 0;
    }

    public getType(): string {
        return "Array";
    }

    public getNodeType(): string {
        return "ArrayNode";
    }

    public getCriteriaString(): string {
        return this.items.length.toString();
    }

    public getOutline(): string {
        if (this.outline === null) {
            var result = "<span class=\"reservedWord\">array</span>(",
                showElements = true,
                omitKeys = true;

            for (var i = 0; i < Math.min(3, this.items.length); i++) {
                showElements = showElements && (this.items[i].getKey().getOutline() !== null) &&
                    (this.items[i].getValue().getOutline() !== null);
                if (!showElements) break;
                omitKeys = omitKeys && ((<PrimitiveNode>this.items[i].getKey()).getValue() === i);
            }
            var parts: string[] = [];
            if (showElements) {
                for (var i = 0; i < Math.min(3, this.items.length); i++)
                    parts.push(((!omitKeys) ? this.items[i].getKey().getOutline() + " => " : "")
                        + this.items[i].getValue().getOutline());
            }
            if (parts !== []) {
                result += parts.join(", ");
                if (this.items.length > 3)
                    result += ", <span class=\"tripleDot\">...</span>";
            } else
                result += "...";
            return result + ")";
        } else
            return this.outline;
    }
}

export class ObjectProperty {
    constructor(protected name: string, protected isStatic: boolean, protected value: Node, protected visibility: string,
        protected declaringClass: string) {
    }

    public getName(): string {
        return this.name;
    }

    public getIsStatic(): boolean {
        return this.isStatic;
    }

    public getValue(): Node {
        return this.value;
    }

    public getVisibility(): string {
        return this.visibility;
    }

    public getDeclaringClass(): string {
        return this.declaringClass;
    }
}

export class ObjectNode extends ComplexNode {
    constructor(protected type: string, protected properties: ObjectProperty[], id: string, nodeReferences: { [id: string]: Node },
        protected outline: string = null) {
        super(id, nodeReferences);
    }

    public getElements(): ObjectProperty[] {
        return this.properties;
    }

    public isEmpty(): boolean {
        return this.properties.length === 0;
    }

    public getType(): string {
        return this.type;
    }

    public getNodeType(): string {
        return "ObjectNode";
    }

    public getCriteriaString(): string {
        return this.properties.length.toString();
    }

    public getOutline(): string {
        if (this.outline === null)
            return Helpers.getOutlineForNamespacedType(this.type);
        else
            return this.outline;
    }
}

//#region TraceValue Nodes

export class Source {
    constructor(protected sourceType: string, protected content: Node) {
    }

    public getSourceType(): string {
        return this.sourceType;
    }

    public getContentNode(): Node {
        return this.content;
    }
}

export class FileSource extends Source {
    constructor(protected path: string, sourceType: string, content: Node) {
        super(sourceType, content);
    }

    public getPath(): string {
        return this.path;
    }
}

export class StackTraceNode extends ComplexNode {
    private resolved = false;

    constructor(protected callFrames: CallFrameNode[], id: string, nodeReferences: { [id: string]: Node }, protected callingLine: string = null) {
        super(id, nodeReferences);
    }

    private resolveCallFrames(): void {
        if (!this.resolved) {
            this.callFrames = this.callFrames.map((callFrame: Node) => {
                while (callFrame instanceof ReferenceNode)
                    callFrame = (<ReferenceNode>callFrame).getReferencedNode();
                return <CallFrameNode>callFrame;
            });
            this.resolved = true;
        }
    }

    public getMostRecentCallFrame(): CallFrameNode {
        this.resolveCallFrames();
        return this.callFrames[0];
    }

    public getLeastRecentCallFrame(): CallFrameNode {
        this.resolveCallFrames();
        return this.callFrames[this.callFrames.length - 1];
    }

    public getElements(): CallFrameNode[] {
        this.resolveCallFrames();
        return this.callFrames;
    }

    public isEmpty(): boolean {
        return this.callFrames.length === 0;
    }

    public getType(): string {
        return "StackTrace";
    }

    public getNodeType(): string {
        return "StackTraceNode";
    }

    public getCriteriaString(): string {
        return this.callFrames.length.toString();
    }

    public getOutline(): string {
        return this.callingLine;
    }
}

export class FunctionCallFrameNode extends CallFrameNode {
    constructor(protected targetFunctionName: string, args: ArrayNode, sourceLine: number, source: Source,
        id: string, nodeReferences: { [id: string]: Node }) {
        super(args, sourceLine, source, id, nodeReferences);
    }

    public getTargetFunctionName(): string {
        return this.targetFunctionName;
    }

    public getType(): string {
        return "FunctionCallFrame";
    }

    public getNodeType(): string {
        return "FunctionCallFrameNode";
    }

    public getCriteriaString(): string {
        return super.getCriteriaString() + this.targetFunctionName;
    }

    public getOutline(): string {
        return Helpers.getOutlineForNamespacedType(this.targetFunctionName, "function") + this.getArgumentsString()
            + Helpers.getOutlineForSource(this.source, this.sourceLine);
    }
}

export class ClosureHandle {
    constructor(private source: Source, private content: PrimitiveNode) {
    }

    public getSource(): Source {
        return this.source;
    }

    public getContent(): PrimitiveNode {
        return this.content;
    }
}

export class ClosureCallFrameNode extends FunctionCallFrameNode {
    constructor(protected targetClosureScopeNamespace: string, protected targetClosureScopeClass: string, protected targetClosureScopeObject: ObjectNode,
        protected targetClosureHandle: ClosureHandle, targetFunctionName: string, args: ArrayNode, sourceLine: number,
        source: Source, id: string, nodeReferences: { [id: string]: Node }) {
        super(targetFunctionName, args, sourceLine, source, id, nodeReferences);
    }

    public getTargetClosureScopeNamespace(): string {
        return this.targetClosureScopeNamespace;
    }

    public getTargetClosureScopeClass(): string {
        return this.targetClosureScopeClass;
    }

    public getTargetClosureScopeObject(): ObjectNode {
        return this.targetClosureScopeObject;
    }

    public getTargetClosureHandle(): ClosureHandle {
        return this.targetClosureHandle;
    }

    public isBound(): boolean {
        return this.targetClosureScopeClass !== null;
    }

    public isStatic(): boolean {
        return this.targetClosureScopeObject === null;
    }

    public getType(): string {
        return "ClosureCallFrame";
    }

    public getNodeType(): string {
        return "ClosureCallFrameNode";
    }

    public getCriteriaString(): string {
        return super.getCriteriaString() + this.targetClosureScopeNamespace + this.targetClosureScopeClass;
    }

    public getOutline(): string {
        var result = "";
        if (this.isBound() && this.isStatic())
            result += Helpers.getOutlineForNamespacedType(this.targetClosureScopeClass)
                + "::<span class=\"function\">" + this.targetFunctionName + "</span>";
        else if (this.isBound() && !this.isStatic())
            result += Helpers.getOutlineForNamespacedType(this.targetClosureScopeClass)
                + "-><span class=\"function\">" + this.targetFunctionName + "</span>";
        else if ((this.targetClosureScopeNamespace !== null) && (this.targetClosureScopeNamespace.length > 0))
            result += Helpers.getOutlineForNamespacedType(this.targetClosureScopeNamespace + "\\" + "<span class=\"function\">" + this.targetFunctionName + "</span>");
        else
            result += "<span class=\"function\">" + this.targetFunctionName + "</span>";
        return result + this.getArgumentsString() + Helpers.getOutlineForSource(this.source, this.sourceLine);
    }
}

export class StaticMethodCallFrameNode extends MethodCallFrameNode {
    public getType(): string {
        return "StaticMethodCallFrame";
    }

    public getNodeType(): string {
        return "StaticMethodCallFrameNode";
    }

    public getCriteriaString(): string {
        return super.getCriteriaString() + "::";
    }

    public getOutline(): string {
        return Helpers.getOutlineForNamespacedType(this.targetClassName)
            + "::<span class=\"function\">" + this.targetMethodName + "</span>" + this.getArgumentsString()
            + Helpers.getOutlineForSource(this.source, this.sourceLine);
    }
}

export class InstanceMethodCallFrameNode extends MethodCallFrameNode {
    constructor(protected targetObject: Node, targetMethodName: string, targetClassName: string, args: ArrayNode,
        sourceLine: number, source: Source, id: string, nodeReferences: { [id: string]: Node }) {
        super(targetMethodName, targetClassName, args, sourceLine, source, id, nodeReferences);
    }

    public getTargetObject(): Node {
        return this.targetObject;
    }

    public getType(): string {
        return "InstanceMethodCallFrame";
    }

    public getNodeType(): string {
        return "InstanceMethodCallFrameNode";
    }

    public getCriteriaString(): string {
        return super.getCriteriaString() + "->";
    }

    public getOutline(): string {
        return Helpers.getOutlineForNamespacedType(this.targetClassName)
            + "-><span class=\"function\">" + this.targetMethodName + "</span>" + this.getArgumentsString()
            + Helpers.getOutlineForSource(this.source, this.sourceLine);
    }
}

//#endregion
//#endregion
