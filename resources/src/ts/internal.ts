/// <reference path="../../typings/index.d.ts" />

import domReady = require("domReady");
import $ = require("jquery");
import velocity = require("velocity");
import JavascriptHtmlDumper = require("./JavascriptHtmlDumper");
import Template = require("./Template");
import NodeFactories = require("./NodeFactories");
import Nodes = require("./Nodes");

domReady(() => {
    var rs = window.ResizeSensor;
    velocity.debug = 0; // 2
    var templatesReady = 0,
        dataReady = 0,
        id = $(window.frameElement).data("id"),
        data: { id: string; root: Nodes.Node; nodes: { [id: string]: Nodes.Node }; trace: Nodes.StackTraceNode; callingLine: string } = null,
        dumper: JavascriptHtmlDumper.JavascriptHtmlDumper = null,
        origin = window.location.origin,
        resizeSensor: ResizeSensorInstance = null,
        container: HTMLElement,
        listener = (event: MessageEvent) => {
            if (event.origin !== origin)
                return;

            if (event.data === "dom-ready") {
                window.containerWindow.postMessage({ type: "send-data", id: window.dumperId }, origin);
            }

            if ((event.data === "templates-ready") && (templatesReady === 0)) {
                templatesReady++;
                (dataReady > 0) ? init() : init("templates");
            }

            if ((event.data === "data-ready") && (dataReady === 0)) {
                dataReady++;
                (templatesReady > 0) ? init() : init("data");
            }
        },
        init = (type: string = null) => {
            if ((type === "templates") || ((type === null) && (templatesReady === 1))) { // Create templates
                Template.getFromText(window.containerWindow.kaemmelotTemplateTexts);
                templatesReady++;
            }
            if ((type === "data") || ((type === null) && (dataReady === 1))) { // Load data
                var div = $("div.data");
                data = NodeFactories.getFromJson(div.eq(0).text());
                div.remove();
                dataReady++;
            }
            if (type === null) { // Complete init
                window.removeEventListener("message", listener);
                container = $("<div>", { class: "container" }).appendTo($("body"))[0];
                dumper = new JavascriptHtmlDumper.JavascriptHtmlDumper(data.root, data.nodes, data.trace, data.callingLine, container, data.id,
                    (rootRepresentation) => new JavascriptHtmlDumper.SessionStorageRepresentationState(rootRepresentation)); // TODO extra worker->async?
                if (window.iframe) {
                    resizeSensor = new rs(container, resize); // Change outer heights on resize
                    $("html").addClass("iframe");
                }
                $("body").attr("style", "height: 24px");
                window.containerWindow.postMessage({ type: "dumper-ready", id: data.id }, origin);
            }

        },
        resize = () => {
            if (container.scrollHeight > 0) {
                var elements = $("body").add(frameElement).add(frameElement.parentElement);
                elements.attr("style", (i, style) => {
                    return style.replace(/height:\s*[0-9]+px/, "height: " + container.scrollHeight + "px");
                });
                var html = $("html")[0];
                var scrollbarSize = container.scrollHeight - html.clientHeight;
                var offset = $(frameElement.parentElement).children("iframe.kaemmelot-dump-control")[0].scrollHeight - (scrollbarSize + container.scrollHeight); // if we are smaller than controls
                if (offset < 0)
                    offset = 0;
                if (scrollbarSize > 0) {
                    elements.attr("style", (i, style) => { // TODO 3 changes? optimize?
                        return style.replace(/height:\s*[0-9]+px/, "height: " + (scrollbarSize + container.scrollHeight) + "px");
                    });
                }
                if (offset > 0) {
                    $(frameElement.parentElement).attr("style", (i, style) => {
                        return style.replace(/height:\s*[0-9]+px/, "height: " + (scrollbarSize + container.scrollHeight + offset) + "px");
                    });
                }
            }
        };
    window.addEventListener("message", listener);
    window.loaded = true;
});
