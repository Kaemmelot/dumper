/// <reference path="../../typings/index.d.ts" />
import $ = require("jquery");

export class Draggable {
    private classes = ["dragMe direction-none", "dragMe direction-vertical", "dragMe direction-horizontal", "dragMe direction-both"]; // const

    private type: number;
    private startX: number;
    private startY: number;
    private containerWidth: number;
    private containerHeight: number;
    private onClickFlag = false;

    public constructor(private xDirection: boolean, private yDirection: boolean, private dragMe: HTMLElement,
        private draggableContainer: HTMLElement, private container: HTMLElement, private allowOverflow = false) {
        this.type = (xDirection ? 1 : 0) + (yDirection ? 1 : 0) * 2;
        $(dragMe).addClass(this.classes[this.type]);
        $(dragMe).on("mousedown", this.onMouseDown);
        $(document).on("mouseup", this.onMouseUp); // Bind on document to prevent bugs (especially for iframes)
        // TODO onscroll without applying it to all parents?
    }

    public getType(): number {
        return this.type;
    }

    public inXDirection(): boolean {
        return this.xDirection;
    }

    public inYDirection(): boolean {
        return this.yDirection;
    }

    public ignoreOnClick(): boolean {
        var result = this.onClickFlag;
        this.onClickFlag = false;
        return result;
    }

    private onMouseDown = (e: JQueryMouseEventObject) => {
        if (e.button !== 0) // only left button
            return;
        this.containerWidth = this.container.clientWidth; // Save container size, so overflow detection works
        this.containerHeight = this.container.clientHeight;
        this.startX = this.getMousePositionX(e.screenX) - (parseInt(this.draggableContainer.style.left, 10) || 0);
        this.startY = this.getMousePositionY(e.screenY) - (parseInt(this.draggableContainer.style.top, 10) || 0);
        this.onClickFlag = false;
        $(document).on("mousemove", this.onMove);
    };

    private onMouseUp = (e: JQueryMouseEventObject) => {
        if (e.button !== 0) // only left button
            return;
        $(document).off("mousemove");
    };

    private onMove = (e: JQueryMouseEventObject) => {
        if (!((<any>e).buttons & 1)) { // prevent bug when left button not pressed anymore
            var event = $.Event("mouseup");
            (<any>event).button = 0; // left button
            $(document).trigger(event);
            return;
        }
        var x = (this.xDirection) ? this.getMousePositionX(e.screenX) - this.startX : (parseInt(this.draggableContainer.style.left, 10) || 0),
            y = (this.yDirection) ? this.getMousePositionY(e.screenY) - this.startY : (parseInt(this.draggableContainer.style.top, 10) || 0);
        if (this.xDirection) {
            if (x < 0)
                x = 0;
            if (!this.allowOverflow && (x + this.draggableContainer.clientWidth > this.containerWidth))
                x = this.containerWidth - this.draggableContainer.clientWidth;
            if (!this.onClickFlag && (x !== (parseInt(this.draggableContainer.style.left, 10) || 0)))
                this.onClickFlag = true;
            this.draggableContainer.style.left = x + "px";
        }
        if (this.yDirection) {
            if (y < 0)
                y = 0;
            if (!this.allowOverflow && (y + this.draggableContainer.clientHeight > this.containerHeight))
                y = this.containerHeight - this.draggableContainer.clientHeight;
            if (!this.onClickFlag && (y !== (parseInt(this.draggableContainer.style.top, 10) || 0)))
                this.onClickFlag = true;
            this.draggableContainer.style.top = y + "px";
        }
        return false;
    };

    // Calculate own position due to buggy behavior with iframes
    private getMousePositionX(screenX: number): number {
        var result = screenX;
        var el = this.draggableContainer;
        while (el) {
            result += el.scrollLeft;
            el = el.parentElement;
        }
        return result;
    }

    private getMousePositionY(screenY: number): number {
        var result = screenY;
        var el = this.draggableContainer;
        while (el) {
            result += el.scrollTop;
            el = el.parentElement;
        }
        return result;
    }
}
