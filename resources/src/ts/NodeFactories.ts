﻿import $ = require("jquery");
import Nodes = require("./Nodes");
import ObjectHelper = require("./ObjectHelper");

export function getFromJson(json: string): { id: string; root: Nodes.Node; nodes: { [id: string]: Nodes.Node }; trace: Nodes.StackTraceNode; callingLine: string } {
    var data: { "@type": string; id: string; root: any; nodes: { [id: string]: any }; trace: any; callingLine: string } = JSON.parse(json);
    if (data["@type"] !== "javascriptHtmlDumper")
        throw "Unsupported type " + data["@type"];

    var nodes: { [name: string]: Nodes.Node } = {};

    var registry: NodeFactoryRegistry = new NodeFactoryRegistry();
    registry.registerFactory(new ObjectNodeFactory(registry, nodes));
    registry.registerFactory(new PrimitiveNodeFactory());
    registry.registerFactory(new ArrayNodeFactory(registry, nodes));
    registry.registerFactory(new UnknownNodeFactory());
    registry.registerFactory(new ReferenceNodeFactory(registry, nodes));
    registry.registerFactory(new AbortDepthNodeFactory());
    registry.registerFactory(new StackTraceFactory(registry, nodes));
    registry.registerFactory(new CallFrameFactory(registry, nodes));

    var root = registry.createFromJson(data.root);
    var trace: Nodes.Node = null;
    var newNodes: { [id: string]: Nodes.Node } = {};
    newNodes[root.getId()] = root;
    if ((data.trace !== null) && (data.trace !== undefined)) {
        trace = registry.createFromJson(data.trace);
        newNodes[trace.getId()] = trace;
    }
    $.extend(nodes, newNodes, ObjectHelper.map(data.nodes, node => registry.createFromJson(node)));
    while (root instanceof Nodes.ReferenceNode)
        root = (<Nodes.ReferenceNode>root).getReferencedNode();
    while ((trace !== null) && (trace instanceof Nodes.ReferenceNode))
        trace = (<Nodes.ReferenceNode>trace).getReferencedNode();

    return { id: data.id, root: root, nodes: nodes, trace: <Nodes.StackTraceNode>trace, callingLine: data.callingLine };
}

//#region Abstract Factories

export class NodeFactory {
    public getAcceptedType(): string {
        return null;
    }

    public createFromJson(json: any): Nodes.Node {
        throw "Abstract function not implemented";
    }
}

export class ComplexNodeFactory extends NodeFactory {
    constructor(private nodeFactory: NodeFactory, private nodeReferences: { [id: string]: Nodes.Node }) {
        super();
    }

    public getNodeFactory(): NodeFactory {
        return this.nodeFactory;
    }

    public getNodeReferences(): { [id: string]: Nodes.Node } {
        return this.nodeReferences;
    }
}

//#endregion

export class NodeFactoryRegistry extends NodeFactory {
    private factories: { [index: string]: NodeFactory; } = {};

    public containsFactoryForType(type: string): boolean {
        return (type in this.factories);
    }

    public getFactoryForType(type: string): NodeFactory {
        return this.factories[type];
    }

    public registerFactory(factory: NodeFactory): void {
        if (this.containsFactoryForType(factory.getAcceptedType()))
            throw "Factory type already registered";
        this.factories[factory.getAcceptedType()] = factory;
    }

    public unregisterFactory(factory: NodeFactory): void {
        if (!this.containsFactoryForType(factory.getAcceptedType()))
            throw "Factory is not yet registered";
        delete this.factories[factory.getAcceptedType()];
    }

    public createFromJson(json: any): Nodes.Node {
        if (!this.containsFactoryForType(json["@type"])) {
            throw "Node type '" + json["@type"] + "' not supported";
        }
        return this.getFactoryForType(json["@type"]).createFromJson(json);
    }
}

//#region Simple Factories

export class AbortDepthNodeFactory extends NodeFactory {
    public createFromJson(json: { "@type": string; id: string; depth: number }): Nodes.Node {
        return new Nodes.AbortDepthNode(json.depth, json.id);
    }

    public getAcceptedType(): string {
        return "abortDepth";
    }
}

export class UnknownNodeFactory extends NodeFactory {
    public createFromJson(json: { "@type": string; id: string; type: string }): Nodes.Node {
        return new Nodes.UnknownNode(json.type, json.id);
    }

    public getAcceptedType(): string {
        return "unknown";
    }
}

class PrimitiveNodeFactory extends NodeFactory {
    public createFromJson(json: { "@type": string; id: string; type: string; value: any }): Nodes.Node {
        return new Nodes.PrimitiveNode(json.type, json.value, json.id);
    }

    public getAcceptedType(): string {
        return "primitive";
    }
}

//#endregion

//#region Complex Factories

export class ReferenceNodeFactory extends ComplexNodeFactory {
    public createFromJson(json: { "@type": string; id: string; referencedId: string }): Nodes.Node {
        return new Nodes.ReferenceNode(json.referencedId, json.id, this.getNodeReferences());
    }

    public getAcceptedType(): string {
        return "reference";
    }
}

export class ArrayNodeFactory extends ComplexNodeFactory {
    public createFromJson(json: { "@type": string; id: string; items: { key: any; value: any }[] }): Nodes.Node {
        var items: Nodes.ArrayElement[] = json.items.map(entry => this.createArrayElementFromJson(entry));
        return new Nodes.ArrayNode(items, json.id, this.getNodeReferences());
    }

    public createArrayElementFromJson(json: { key: any; value: any }) {
        return new Nodes.ArrayElement(this.getNodeFactory().createFromJson(json.key), this.getNodeFactory().createFromJson(json.value));
    }

    public getAcceptedType(): string {
        return "array";
    }
}

export class ObjectNodeFactory extends ComplexNodeFactory {
    public createFromJson(json: {
        "@type": string; id: string; type: string; properties: {
            name: string; isStatic: boolean; declaringClass: string; visibility: string; value: any
        }[]; outline: string
    }): Nodes.Node {
        var properties: Nodes.ObjectProperty[] = json.properties.map(prop => this.createObjectPropertyFromJson(prop));
        return new Nodes.ObjectNode(json.type, properties, json.id, this.getNodeReferences(), json.outline);
    }

    public createObjectPropertyFromJson(json: {
        name: string; isStatic: boolean; declaringClass: string; visibility: string; value: any
    }): Nodes.ObjectProperty {
        return new Nodes.ObjectProperty(json.name, json.isStatic, this.getNodeFactory().createFromJson(json.value),
            json.visibility, json.declaringClass);
    }

    public getAcceptedType(): string {
        return "object";
    }
}

//#region TraceValue Factories

export class StackTraceFactory extends ComplexNodeFactory {
    public createFromJson(json: { "@type": string; id: string; callFrames: string[] }): Nodes.Node {
        var callFrames = json.callFrames.map((callFrame) => {
            return <Nodes.CallFrameNode>this.getNodeFactory().createFromJson(callFrame);
        });
        return new Nodes.StackTraceNode(callFrames, json.id, this.getNodeReferences());
    }

    public getAcceptedType(): string {
        return "stackTrace";
    }
}

export class CallFrameFactory extends ComplexNodeFactory {
    public createFromJson(json: { "@type": string; "type": string; id: string; content: { [key: string]: any } }): Nodes.Node {
        var nodeFactory = this.getNodeFactory(),
            args = <Nodes.ArrayNode>nodeFactory.createFromJson(json.content["arguments"]),
            sourceLine: number = json.content["sourceLine"],
            sourceType: string = json.content["sourceType"],
            source: Nodes.Source = null;
        if (json.content.hasOwnProperty("source")) {
            if (json.content["source"]["sourceType"] === "FileSource")
                source = new Nodes.FileSource(json.content["source"]["path"], json.content["source"]["sourceType"],
                    nodeFactory.createFromJson(json.content["source"]["content"]));
            else
                source = new Nodes.Source(json.content["source"]["sourceType"], nodeFactory.createFromJson(json.content["source"]["content"]));
        }
        switch (json.type) {
            case "FunctionCallFrame":
                return new Nodes.FunctionCallFrameNode(json.content["targetFunctionName"], args, sourceLine, source,
                    json.id, this.getNodeReferences());
            case "ClosureCallFrame":
                var targetClosureScopeObject = json.content["targetClosureScopeObject"] !== null ?
                    <Nodes.ObjectNode>nodeFactory.createFromJson(json.content["targetClosureScopeObject"]) : null;
                var closureHandle: Nodes.ClosureHandle = null;
                if (json.content.hasOwnProperty("targetClosureHandleSource")) {

                    if (json.content["targetClosureHandleSource"]["sourceType"] === "FileSource")
                        closureHandle = new Nodes.ClosureHandle(
                            new Nodes.FileSource(json.content["targetClosureHandleSource"]["path"],
                                json.content["targetClosureHandleSource"]["sourceType"],
                                nodeFactory.createFromJson(json.content["targetClosureHandleSource"]["content"])),
                            <Nodes.PrimitiveNode>nodeFactory.createFromJson(json.content["targetClosureHandleContent"]));
                    else
                        closureHandle = new Nodes.ClosureHandle(
                            new Nodes.Source(json.content["targetClosureHandleSource"]["sourceType"],
                                nodeFactory.createFromJson(json.content["targetClosureHandleSource"]["content"])),
                            <Nodes.PrimitiveNode>nodeFactory.createFromJson(json.content["targetClosureHandleContent"]));
                }
                return new Nodes.ClosureCallFrameNode(json.content["targetClosureScopeNamespace"], json.content["targetClosureScopeClass"],
                    targetClosureScopeObject, closureHandle, json.content["targetFunctionName"], args, sourceLine, source,
                    json.id, this.getNodeReferences());
            case "InstanceMethodCallFrame":
                return new Nodes.InstanceMethodCallFrameNode(nodeFactory.createFromJson(json.content["targetObject"]),
                    json.content["targetMethodName"], json.content["targetClassName"], args, sourceLine, source,
                    json.id, this.getNodeReferences());
            case "StaticMethodCallFrame":
                return new Nodes.StaticMethodCallFrameNode(json.content["targetMethodName"], json.content["targetClassName"],
                    args, sourceLine, source, json.id, this.getNodeReferences());
            default:
                throw "Unknown call type '" + json.type + "'";
        }
    }

    public getAcceptedType(): string {
        return "callFrame";
    }
}

//#endregion
//#endregion
