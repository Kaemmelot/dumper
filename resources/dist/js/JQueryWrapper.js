// Don't mess up with other JQuerys
// This wrapper saves the previous loaded JQuery, loads the next JQuery and replaces the global JQuery by the previous
function JQueryWrapper($, jQuery) {
    this.$ = $;
    this.jQuery = jQuery;
}
JQueryWrapper.prototype.publishNewJQuery = true; // TODO config
JQueryWrapper.prototype.restore = function restore(jq) {
    var newJQ = jq.noConflict(true);
    window.$ = this.$;
    window.jQuery = this.jQuery;
    if (this.publishNewJQuery)
        window.kaemmelotDumperJQuery = newJQ; // make our JQuery accessible
    return newJQ;
};
kaemmelotDefine(['jquery'], (function () {
    var wrapper = new JQueryWrapper(window.$, window.jQuery); // keep 'this'
    var def = window.define; // make requirejs accessible for jquery only
    window.define = window.kaemmelotDefine;
    function wrap(jq) {
        window.define = def;
        return wrapper.restore(jq);
    }
    return wrap;
})());

//# sourceMappingURL=index.php?kaemmelot-dumper-resource=dist/js/JQueryWrapper.js.map
